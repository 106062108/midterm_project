# Software Studio 2019 Spring Midterm Project
## Notice
* Replace all [xxxx] to your answer

## Topic
* Project Name : Shopping Web Page
* Key functions (add/delete)
    1. Signin Page (signin.html) : allow user to signin, signin with google, and register a new account
	2. Home Page (index.html) : allow user to browse the goods list and add goods into the shopping cart, to delete goods in the shopping cart, and checkout the shopping cart
	3. Payment Page (payment.html) : allow user to view the shopping cart which has just been checked out
	4. Dashboard Page (dashboard.html) : allow user to modify user information, and allow user to redirect to Historical Record Page and Good Selling Page
	5. Historical Record Page (historical_record.html) : allow user to view historical checked out shopping cart records
	6. Good Selling Page (good_selling.html) : allow user to add new goods to sell, and to modify previously added selling goods
    
* Other functions (add/delete)

## Basic Components
|Component|Score|Y/N|
|:-:|:-:|:-:|
|Membership Mechanism|20%|Y|
|Firebase Page|5%|Y|
|Database|15%|Y|
|RWD|15%|Y|
|Topic Key Function|15%|Y|

## Advanced Components
|Component|Score|Y/N|
|:-:|:-:|:-:|
|Third-Party Sign In|2.5%|Y|
|Chrome Notification|5%|Y|
|Use CSS Animation|2.5%|Y|
|Security Report|5%|Y|

## Website Detail Description

# 作品網址：https://midterm-project-106062108.firebaseapp.com

# Components Description : 
1. Signin Page :
   * a. User can signin with an existed account, and signin with google with an existed google account. However, if signin with google as an existed email address, the account of that email address will be change into a google account with old data.
   * b. User can register a new account with a validly formatted email address if the email address has not been created before.
   * c. There will be a chrome notification request if the browser hasn't responded the request before, and will also be a chrome notification if user grant the request.
2. Home Page :
   * a. User can view the goods list anf add goods into the shopping cart. However, sold out goods or the goods selling by the user cannot be added into the shopping cart. Invalid selected quantity will also be blocked.
   * b. User can view the shopping cart and delete the selected goods in the shopping cart. User can also checkout the shopping cart, and it will automatically redirect to Payment Page. However, the shopping cart will be cleaned up if user leave or reload the page.
3. Payment Page :
   * a. User can view the shopping cart which has just been checked out, and user will get an unique randomly payment serial number.
   
4. Dashboard Page :
   * a. User can modify user information, and the page will be automatically reloaded after the confirm button is clicked.
   * b. User can go to Historical Record Page and Good Selling Page by clicking the corredponding buttoon.
   
5. Historical Record Page :
   * a. User can view historical checked out shopping cart records. The total price and the payment serial number will also be shown.
   
6. Good Selling Page :
   * a. User can add new goods to sell in New Good Sells. However, the confirm request will be blocked if any required column is invalid or uncompleted.
   * b. User can modifty or delete previously added selling goods in Good Sells Record List. However, the confirm request will be blocked if any required column is invalid or uncompleted.
   * c. User can dynamically add or delete good descriptions when adding new selling goods or modiftying previously added selling goods.

# Other Functions Description(1~10%) : 

## Security Report (Optional)
1. Signin Page :
   * a. User cannot autonomously redirect to other pages expect Signin Page and Home Page by clicking the dropdown links.
   
2. Home Page :
   * a. User cannot autonomously redirect to other pages expect Signin Page and Home Page by clicking the dropdown links if user is not signed in.
   * b. User can view the goods list whether user is signed in or not. However, user cannot use the shopping cart (besides adding goods into the shopping cart, and checking out the shopping cart) if user is not signed in.
   * c. The payment serial number will be created as an unique random number when user is browsing Home Page, and it will be deliever to the database when user click the checkout button. Thus, we can handle an existed payment serial number list in the database to avoid from creating the same number at the next time.
3. Payment Page :
   * a. User cannot autonomously redirect to other pages expect Signin Page and Home Page by clicking the dropdown links if user is not signed in.
   * b. User cannot view the page if user is not signed in.
   
4. Dashboard Page :
   * a. User cannot autonomously redirect to other pages expect Signin Page and Home Page by clicking the dropdown links if user is not signed in.
   * b. User cannot view the page if user is not signed in.
   
5. Historical Record Page :
   * a. User cannot autonomously redirect to other pages expect Signin Page and Home Page by clicking the dropdown links if user is not signed in.
   * b. User cannot view the page if user is not signed in.
   
6. Good Selling Page :
   * a. User cannot autonomously redirect to other pages expect Signin Page and Home Page by clicking the dropdown links if user is not signed in.
   * b. User cannot view the page if user is not signed in.
   * c. The good id will be created as an unique random number when user is browsing the Good Selling Page, and it will be deliever to the database when user click the confirm button of New Good Sells. Thus, we can handle an existed good id list in the database to avoid from creating the same number at the next time.
   