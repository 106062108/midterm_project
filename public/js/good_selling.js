const good_serial_number_length = 20;
const good_serial_number_random_base = 26;
const random_number_combination_upper_limit = Math.pow(good_serial_number_random_base, good_serial_number_length);

var new_good_sells_good_serial_number_list = [];
var new_good_sells_good_serial_number = undefined;

var userEmail = undefined;

window.onload = function() {
    init();
}

// load new good sells good serial number list
function load_new_good_sells_good_serial_number_list() {
    return newPromise = new Promise((resolve, reject) => {
        firebase.database().ref('good_serial_number_list').once('value').then(function(snapshot) {
            snapshot.forEach(function(childsnapshot) {
                new_good_sells_good_serial_number_list.push(childsnapshot.val().good_serial_number);
            });
            // console.log("load_new_good_sells_good_serial_number_list() success");
            resolve('success');
        }).catch(function(error) {
            // console.log(error);
            reject('fail');
        });
    });
}

// test new new good sells good serial number
function test_new_new_good_sells_good_serial_number(new_random_serial_number) {
    return new Promise((resolve, reject) => {
        var random_number_combination_counter = 0;
        var serial_number_existed;
        do {
            serial_number_existed = false;
            if(new_good_sells_good_serial_number_list.length == 0) {
                // console.log("test_new_new_good_sells_good_serial_number() success");
                resolve('success');
            }
            else {
                for(var i = 0; i < new_good_sells_good_serial_number_list.length; i++) {
                    if(new_random_serial_number == new_good_sells_good_serial_number_list[i]) {
                        serial_number_existed = true;
                        new_random_serial_number = Math.random().toString(good_serial_number_random_base).substring(2, (2+good_serial_number_length));
                    }
    
                    if(i == new_good_sells_good_serial_number_list.length-1) {
                        if(serial_number_existed == true) {
                            reject('fail');
                        }
                        else {
                            // console.log("test_new_new_good_sells_good_serial_number() success");
                            resolve('success');
                        }
                    }
                }
            }

            random_number_combination_counter++;
            if(random_number_combination_counter == random_number_combination_upper_limit) {
                // console.log("random serial number exhausted");
                reject('fail');
            }
        } while((serial_number_existed == true) && (random_number_combination_counter < random_number_combination_upper_limit));
    });
}

// new good sells good serial number generator
async function new_good_sells_good_serial_number_generator() {
    try {
        var load_new_good_sells_good_serial_number_list_promise = await load_new_good_sells_good_serial_number_list();
        if(load_new_good_sells_good_serial_number_list_promise == 'success') {
            var new_random_serial_number_seed = Math.random().toString(good_serial_number_random_base).substring(2, (2+good_serial_number_length));
            var new_random_serial_number_array = [];
            var new_random_serial_number;
            for(var i = 0; i < new_random_serial_number_seed.length; i++) {
                new_random_serial_number_array.push((parseInt(new_random_serial_number_seed[i], 26) + 10).toString(36));
            }
            new_random_serial_number = new_random_serial_number_array.join('');

            try {
                var test_new_new_good_sells_good_serial_number_promise = await test_new_new_good_sells_good_serial_number(new_random_serial_number);
                if(test_new_new_good_sells_good_serial_number_promise == 'success') {
                    // console.log("new_good_sells_good_serial_number_generator() success : " + new_random_serial_number);
                    new_good_sells_good_serial_number = new_random_serial_number;
                }
            }
            catch(error) {
                new_good_sells_good_serial_number = undefined;
                // console.log(error);
            }
        }
    }
    catch(error) {
        new_good_sells_good_serial_number = undefined;
        // console.log(error);
    }
}

// init
function init() {
    var signout_btn_clicked = false;
    // check user's sign state
    firebase.auth().onAuthStateChanged(function (user) {
        var account = document.getElementById('side-bar-account');
        var shopping_cart = document.getElementById('side-bar-shopping-cart');
        var good_selling_body = document.getElementById('good-selling-page-body-container');
        if(user) {
            // if user is in signin state
            userEmail = user.email;
            account.innerHTML = '<a class="nav-link dropdown-toggle" data-toggle="dropdown">' + userEmail + '</a>' + 
                                '<div id="side-bar-account-menu" class="dropdown-menu">' + 
                                  '<a id="homepage-btn" class="dropdown-item">Home</a>' + 
                                  '<a id="dashboard-btn" class="dropdown-item">Dashboard</a>' + 
                                  '<a id="signout-btn" class="dropdown-item">Signout</a>' + 
                                '</div>';
            shopping_cart.style.visibility = 'visible';
            good_selling_body.style.visibility = 'visible';
            
            // add event listener to homepage button
            var homepage_btn = document.getElementById('homepage-btn');
            homepage_btn.addEventListener('click', function() {
                window.location.assign("index.html");
            });

            // add event listener to dashboard button
            var dashboard_btn = document.getElementById('dashboard-btn');
            dashboard_btn.addEventListener('click', function() {
                window.location.assign("dashboard.html");
            });

            // add event listener to signout button
            var signout_btn = document.getElementById('signout-btn');
            signout_btn.addEventListener('click', function() {
                firebase.auth().signOut().then(function() {
                    signout_btn_clicked = true;
                    create_custom_alert('success', 'Successfully signout. Automatically redirecting to the Signin Page.')
                    setTimeout(function() {
                        window.location.assign("signin.html");
                    }, 1000);
                }).catch(function(error) {
                    var errorCode= error.code;
                    var errorMessage= error.message;
                    create_custom_alert('error', errorMessage);
                });
            });

            // create a random good serial number that each character is selected from {a~z}
            new_good_sells_good_serial_number_generator();

            // load new good sells good seller information
            var new_good_sells_good_seller = document.getElementById('good-seller');
            new_good_sells_good_seller.value = userEmail;
            
            // load good sells record list
            var goodListRef = firebase.database().ref('good_list');
            var breadGoodListRef = goodListRef.child('bread');
            var cakeGoodListRef = goodListRef.child('cake');
            var snackGoodListRef = goodListRef.child('snack');
            var beverageGoodListRef = goodListRef.child('beverage');
            var otherGoodListRef = goodListRef.child('other');
			
            load_classification_good_list(breadGoodListRef, userEmail);
            load_classification_good_list(cakeGoodListRef, userEmail);
            load_classification_good_list(snackGoodListRef, userEmail);
            load_classification_good_list(beverageGoodListRef, userEmail);
            load_classification_good_list(otherGoodListRef, userEmail);
        }
        else {
            // if user is in signout state
            account.innerHTML = '<a class="nav-link dropdown-toggle" data-toggle="dropdown">Guest</a>' + 
                                '<div id="side-bar-account-menu" class="dropdown-menu">' + 
                                  '<a id="homepage-btn" class="dropdown-item" href="index.html">Home</a>' + 
                                  '<a id="signin-btn" class="dropdown-item" href="signin.html">Signin</a>' + 
                                '</div>';
            shopping_cart.style.visibility = 'hidden';
            good_selling_body.style.visibility = 'hidden';
            if(!signout_btn_clicked) {
                create_authorization_alert('error', 'You should signin first to view this page.')
            }
        }
    });

    // add event listener to new good sells image button
    var new_good_sells_image_btn = document.getElementById('new-good-sells-image-btn');
    new_good_sells_image_btn.addEventListener('click', function() {
        var new_good_sells_image_file_reader = document.getElementById('new-good-sells-image-file-reader');
        new_good_sells_image_file_reader.click();
    });

    // add event listener to new good sells image file reader
    var new_good_sells_image_file_reader = document.getElementById('new-good-sells-image-file-reader');
    new_good_sells_image_file_reader.addEventListener('change', function() {
        var new_good_sells_image = document.getElementById('new-good-sells-image');
        var file = new_good_sells_image_file_reader.files[0];
        var file_reader = new FileReader();

        if(file) {
            file_reader.readAsDataURL(file);
        }

        file_reader.addEventListener('load', function() {
            new_good_sells_image.src = file_reader.result;
        }, false);
    });

    // add event listener to new good sells description button
    var new_good_sells_description_add_btn = document.getElementById('new-good-sells-description-add-btn');
    new_good_sells_description_add_btn.addEventListener('click', function() {
        var new_good_sells_description_list = document.getElementById('new-good-sells-description-list-container');
        var new_good_sells_description_list_length = parseInt(new_good_sells_description_list.getAttribute('list_length'), 10);
        new_good_sells_description_list.setAttribute('list_length', (new_good_sells_description_list_length + 1));

        // record the text of the description list
        var new_good_sells_description_element_list = document.getElementsByClassName('new-good-sells-description');
        var new_good_sells_description_text_list = [];
        for(var i = 0; i < new_good_sells_description_element_list.length; i++) {
            new_good_sells_description_text_list.push(new_good_sells_description_element_list[i].value);
        }
        
        var new_description_html = new_good_sells_description_html_code_generator(new_good_sells_description_list_length + 1);
        new_good_sells_description_list.innerHTML += new_description_html;

        // recover the text of the description list
        for(var i = 0; i < new_good_sells_description_text_list.length; i++) {
            new_good_sells_description_element_list[i].value = new_good_sells_description_text_list[i];
        }
    });

    var new_good_sells_description_delete_btn = document.getElementById('new-good-sells-description-delete-btn');
    new_good_sells_description_delete_btn.addEventListener('click', function() {
        var new_good_sells_description_list = document.getElementById('new-good-sells-description-list-container');
        var new_good_sells_description_list_length = parseInt(new_good_sells_description_list.getAttribute('list_length'), 10);
        if(new_good_sells_description_list_length > 1) {
            new_good_sells_description_list.setAttribute('list_length', (new_good_sells_description_list_length - 1));
            
            // record the text of the description list
            var new_good_sells_description_element_list = document.getElementsByClassName('new-good-sells-description');
            var new_good_sells_description_text_list = [];
            for(var i = 0; i < new_good_sells_description_element_list.length; i++) {
                new_good_sells_description_text_list.push(new_good_sells_description_element_list[i].value);
            }

            var delete_description_html = new_good_sells_description_html_code_generator(new_good_sells_description_list_length);
            new_good_sells_description_list.innerHTML = new_good_sells_description_list.innerHTML.replace(delete_description_html, '');

            // recover the text of the description list
            for(var i = 0; i < new_good_sells_description_element_list.length; i++) {
                new_good_sells_description_element_list[i].value = new_good_sells_description_text_list[i];
            }
        }
    });

    // add event listener to new good sells confirm button
    var new_good_sells_confirm_btn = document.getElementById('new-good-sells-confirm-btn');
    new_good_sells_confirm_btn.addEventListener('click', function() {
        // get new good sells information
        var new_good_sells_image_url = document.getElementById('new-good-sells-image').src;
        var new_good_sells_name = document.getElementById('new-good-sells-name').value;
        var new_good_sells_price = document.getElementById('new-good-sells-price').value;
        var new_good_sells_num = document.getElementById('new-good-sells-num').value;
        var new_good_sells_classification = document.getElementById('new-good-sells-classification').value;
        var new_good_sells_seller = document.getElementById('good-seller').value;
        var new_good_sells_description_list = [];
        var new_good_sells_description_list_element_set = document.getElementsByClassName('new-good-sells-description');
        for(var i = 0; i < new_good_sells_description_list_element_set.length; i++) {
            new_good_sells_description_list.push(new_good_sells_description_list_element_set[i].value);
        }

        if(new_good_sells_good_serial_number == undefined) {
            create_custom_alert('error', 'Server is busy now. Please try it again later.')
        }
        else {
            if(new_good_sells_image_url == '' || new_good_sells_name == '' || new_good_sells_price == '' || new_good_sells_num == '') {
                var new_good_sells_title = document.getElementById('new-good-sells-title').innerHTML;
                create_custom_alert('error', 'Please complete all required columns of "' + new_good_sells_title + '".');
            }
            else {
                var goodListRef = firebase.database().ref('good_list');
                var classificationGoodListRef = goodListRef.child(new_good_sells_classification);
                var newGoodRef = classificationGoodListRef.push();
                newGoodRef.set({
                    good_id: new_good_sells_good_serial_number,
                    good_image_url: new_good_sells_image_url,
                    good_name: new_good_sells_name,
                    good_price: new_good_sells_price,
                    good_num: new_good_sells_num,
                    good_seller: new_good_sells_seller,
                    good_description_list: new_good_sells_description_list
                });

                var goodSerialNumberListRef = firebase.database().ref('good_serial_number_list');
                var newGoodSerialNumberRef = goodSerialNumberListRef.push();
                newGoodSerialNumberRef.set({
                    good_serial_number: new_good_sells_good_serial_number
                });

                create_custom_alert('success', 'Successfully deliver your new selling good information to the server. Automatically reloading the current page.');
                setTimeout(function(){
                    window.location.reload();
                }, 1000);
            }
        }
    });

    // add event listener to good selling return button
    var good_selling_record_return_btn = document.getElementById('good-selling-return-btn');
    good_selling_record_return_btn.addEventListener('click', function() {
        window.location.assign("dashboard.html");
    });
}

// load classification good list
function load_classification_good_list(classificationGoodListRef, userEmail) {
    // load classification good list
    classificationGoodListRef.once('value').then(function(snapshot) {
        document.getElementById('good-sells-record-list').style.visibility = 'visible';
        
        var good_sells_record_list_null = true;
        snapshot.forEach(function(childsnapshot) {
            if(childsnapshot.val().good_seller == userEmail) {
                good_sells_record_list_null = false;

                var good_id = childsnapshot.val().good_id;
                var good_image_url = childsnapshot.val().good_image_url;
                var good_name = childsnapshot.val().good_name;
                var good_price = childsnapshot.val().good_price;
                var good_num = childsnapshot.val().good_num;
                var good_classification = snapshot.key;
                var good_seller = childsnapshot.val().good_seller;
                var good_description_list = childsnapshot.val().good_description_list;
                var good_buyer_list = childsnapshot.val().good_buyer_list;
                var good_sells_record = good_sells_record_html_code_generator(good_id, good_image_url, good_name, good_price, good_num, good_classification, good_seller, good_description_list, good_buyer_list);

                // update good sells record list html code
                var good_sells_record_list = document.getElementById('good-sells-record-list');
                good_sells_record_list.innerHTML += good_sells_record;
                /*
                // add event listener to good sells record image button
                var good_sells_record_image_button_str = good_id + ' good-sells-record-image-btn';
                var good_sells_record_image_button = document.getElementsByClassName(good_sells_record_image_button_str)[0];
                good_sells_record_image_button.addEventListener('click', function() {
                    var good_sells_record_image_file_reader_str = good_id + ' good-sells-record-image-file-reader';
                    var good_sells_record_image_file_reader = document.getElementsByClassName(good_sells_record_image_file_reader_str)[0];
                    good_sells_record_image_file_reader.click();
                });
                */
                
                /*
                // add event listener to good sells record image file reader
                var good_sells_record_image_file_reader_str = good_id + ' good-sells-record-image-file-reader';
                var good_sells_record_image_file_reader = document.getElementsByClassName(good_sells_record_image_file_reader_str)[0];
                good_sells_record_image_file_reader.addEventListener('change', function() {
                    var good_sells_record_image_str = good_id + ' good-sells-record-image';
                    var good_sells_record_image = document.getElementsByClassName(good_sells_record_image_str)[0];
                    var file = good_sells_record_image_file_reader.files[0];
                    var file_reader = new FileReader();

                    if(file) {
                        file_reader.readAsDataURL(file);
                    }

                    file_reader.addEventListener('load', function() {
                        good_sells_record_image.src = file_reader.result;
                    }, false);
                });
                */
                
                /*
                // add event listener to good sells record description button
                var good_sells_record_description_add_btn_str = good_id + ' good-sells-record-description-add-btn';
                var good_sells_record_description_add_btn = document.getElementsByClassName(good_sells_record_description_add_btn_str)[0];
                good_sells_record_description_add_btn.addEventListener('click', function() {
                    var good_sells_record_description_list_str = good_id + ' good-sells-record-description-list-container';
                    var good_sells_record_description_list = document.getElementsByClassName(good_sells_record_description_list_str)[0];
                    var good_sells_record_description_list_length = parseInt(good_sells_record_description_list.getAttribute('list_length'), 10);
                    good_sells_record_description_list.setAttribute('list_length', (good_sells_record_description_list_length + 1));

                    // record the text of the description list
                    var good_sells_record_description_element_list_str = good_id + ' good-sells-record-description';
                    var good_sells_record_description_element_list = document.getElementsByClassName(good_sells_record_description_element_list_str);
                    var good_sells_record_description_text_list = [];
                    for(var i = 0; i < good_sells_record_description_element_list.length; i++) {
                        good_sells_record_description_text_list.push(good_sells_record_description_element_list[i].value);
                    }

                    var good_sells_record_description_value = '';
                    
                    var new_description_html = good_sells_record_description_html_code_generator(good_id, good_sells_record_description_list_length + 1, good_sells_record_description_value);
                    good_sells_record_description_list.innerHTML += new_description_html;

                    // recover the text of the description list
                    for(var i = 0; i < good_sells_record_description_text_list.length; i++) {
                        good_sells_record_description_element_list[i].value = good_sells_record_description_text_list[i];
                    }
                });

                var good_sells_record_description_delete_btn_str = good_id + ' good-sells-record-description-delete-btn';
                var good_sells_record_description_delete_btn = document.getElementsByClassName(good_sells_record_description_delete_btn_str)[0];
                good_sells_record_description_delete_btn.addEventListener('click', function() {
                    var good_sells_record_description_list_str = good_id + ' good-sells-record-description-list-container';
                    var good_sells_record_description_list = document.getElementsByClassName(good_sells_record_description_list_str)[0];
                    var good_sells_record_description_list_length = parseInt(good_sells_record_description_list.getAttribute('list_length'), 10);
                    if(good_sells_record_description_list_length > 1) {
                        good_sells_record_description_list.setAttribute('list_length', (good_sells_record_description_list_length - 1));

                        // record the text of the description list
                        var good_sells_record_description_element_list_str = good_id + ' good-sells-record-description';
                        var good_sells_record_description_element_list = document.getElementsByClassName(good_sells_record_description_element_list_str);
                        var good_sells_record_description_text_list = [];
                        for(var i = 0; i < good_sells_record_description_element_list.length; i++) {
                            good_sells_record_description_text_list.push(good_sells_record_description_element_list[i].value);
                        }

                        var good_sells_record_description_value = document.getElementsByClassName(good_id + ' description_' + good_sells_record_description_list_length + ' good-sells-record-description')[0].value;
                        
                        var delete_description_html = good_sells_record_description_html_code_generator(good_id, good_sells_record_description_list_length, good_sells_record_description_value);
                        good_sells_record_description_list.innerHTML = good_sells_record_description_list.innerHTML.replace(delete_description_html, '');

                        // recover the text of the description list
                        for(var i = 0; i < good_sells_record_description_element_list.length; i++) {
                            good_sells_record_description_element_list[i].value = good_sells_record_description_text_list[i];
                        }
                    }
                });
                */

                /*
                // add event listener to good sells record delete button
                var good_sells_record_delete_btn_str = good_id + ' good-sells-record-delete-btn';
                var good_sells_record_delete_btn = document.getElementsByClassName(good_sells_record_delete_btn_str)[0];
                good_sells_record_delete_btn.addEventListener('click', function() {
                    var childsnapshotRef = childsnapshot.ref;
                    childsnapshotRef.remove();

                    good_sells_record_list.innerHTML.replace(good_sells_record, '');

                    // remove good_id from good_serial_number_list
                    var goodSerialNumberListRef = firebase.database().ref('good_serial_number_list');
                    goodSerialNumberListRef.once('value').then(function(snapshot) {
                        snapshot.forEach(function(childsnapshot) {
                            if(childsnapshot.val().good_serial_number == good_id) {
                                var childsnapshotRef = childsnapshot.ref;
                                childsnapshotRef.remove();
                            }
                        });
                    }).catch(function(error) {
                        var errorCode= error.code;
                        var errorMessage= error.message;
                        create_custom_alert('error', errorMessage);
                    });

                    create_custom_alert('success', 'Successfully delete your selling good information from the server. Automatically reloading the current page.');
                    setTimeout(function(){
                        window.location.reload();
                    }, 1000);
                });
                */
                /*
                // add event listener to good sells record confirm button
                var good_sells_record_confirm_btn_str = good_id + ' good-sells-record-confirm-btn';
                var good_sells_record_confirm_btn = document.getElementsByClassName(good_sells_record_confirm_btn_str)[0];
                
                good_sells_record_confirm_btn.addEventListener('click', function() {
                    var new_good_sells_record_image_url = document.getElementsByClassName(good_id + ' good-sells-record-image')[0].src;
                    var new_good_sells_record_name = document.getElementsByClassName(good_id + ' good-sells-record-name')[0].value;
                    var new_good_sells_record_price = document.getElementsByClassName(good_id + ' good-sells-record-price')[0].value;
                    var new_good_sells_record_num = document.getElementsByClassName(good_id + ' good-sells-record-num')[0].value;
                    var new_good_sells_record_description_list = [];
                    var new_good_sells_record_description_element_list = document.getElementsByClassName(good_id + ' good-sells-record-description');
                    for(var i = 0; i < new_good_sells_record_description_element_list.length; i++) {
                        new_good_sells_record_description_list.push(new_good_sells_record_description_element_list[i].value);
                    }

                    if(new_good_sells_record_image_url == '' || new_good_sells_record_name == '' || new_good_sells_record_price == '' || new_good_sells_record_num == '') {
                        var good_sells_record_name = childsnapshot.val().good_name;
                        create_custom_alert('error', 'Please complete all required columns of the good "' + good_sells_record_name + '".');
                    }
                    else {
                        childsnapshot.ref.update({
                            good_image_url: new_good_sells_record_image_url,
                            good_name: new_good_sells_record_name,
                            good_price: new_good_sells_record_price,
                            good_num: new_good_sells_record_num,
                            good_description_list: new_good_sells_record_description_list
                        });

                        create_custom_alert('success', 'Successfully update your selling good information to the server. Automatically reloading the current page.');
                        setTimeout(function(){
                            window.location.reload();
                        }, 1000);
                    }
                });
                */
            }
        });

        if(good_sells_record_list_null == true) {
            document.getElementById('good-sells-record-list-null-alert').style.display = 'block';
        }
        document.getElementById('good-sells-record-list-loading-alert').style.display = 'none';
        document.getElementById('good-sells-record-list').style.visibility = 'visible';
    }).catch(function(error) {
        var errorCode= error.code;
        var errorMessage= error.message;
        create_custom_alert('error', errorMessage);
    });
}

// add event listener to good sells record image button
function good_sells_record_image_button_onclick(good_id) {
    // console.log("good_sells_record_image_button_onclick()");

    var good_sells_record_image_file_reader_str = good_id + ' good-sells-record-image-file-reader';
    var good_sells_record_image_file_reader = document.getElementsByClassName(good_sells_record_image_file_reader_str)[0];
    good_sells_record_image_file_reader.click();
}

// add event listener to good sells record image file reader
function good_sells_record_image_file_reader_onchange(good_id) {
    // console.log("good_sells_record_image_file_reader_onchange()");
    var good_sells_record_image_file_reader_str = good_id + ' good-sells-record-image-file-reader';
    var good_sells_record_image_file_reader = document.getElementsByClassName(good_sells_record_image_file_reader_str)[0];
    var good_sells_record_image_str = good_id + ' good-sells-record-image';
    var good_sells_record_image = document.getElementsByClassName(good_sells_record_image_str)[0];
    var file = good_sells_record_image_file_reader.files[0];
    var file_reader = new FileReader();

    if(file) {
        file_reader.readAsDataURL(file);
    }

    file_reader.addEventListener('load', function() {
        good_sells_record_image.src = file_reader.result;
    }, false);
}

// add event listener to good sells record description add button
function good_sells_record_description_add_button_onclick(good_id) {
    // console.log("good_sells_record_description_add_button_onclick()");
    
    var good_sells_record_description_list_str = good_id + ' good-sells-record-description-list-container';
    var good_sells_record_description_list = document.getElementsByClassName(good_sells_record_description_list_str)[0];
    var good_sells_record_description_list_length = parseInt(good_sells_record_description_list.getAttribute('list_length'), 10);
    good_sells_record_description_list.setAttribute('list_length', (good_sells_record_description_list_length + 1));

    // record the text of the description list
    var good_sells_record_description_element_list_str = good_id + ' good-sells-record-description';
    var good_sells_record_description_element_list = document.getElementsByClassName(good_sells_record_description_element_list_str);
    var good_sells_record_description_text_list = [];
    for(var i = 0; i < good_sells_record_description_element_list.length; i++) {
        good_sells_record_description_text_list.push(good_sells_record_description_element_list[i].value);
    }

    var good_sells_record_description_value = '';
        
    var new_description_html = good_sells_record_description_html_code_generator(good_id, good_sells_record_description_list_length + 1, good_sells_record_description_value);
    good_sells_record_description_list.innerHTML += new_description_html;

    // recover the text of the description list
    for(var i = 0; i < good_sells_record_description_text_list.length; i++) {
        good_sells_record_description_element_list[i].value = good_sells_record_description_text_list[i];
    }
}

// add event listener to good sells record description delete button
function good_sells_record_description_delete_button_onclick(good_id) {
    // console.log("good_sells_record_description_delete_button_onclick()");
    
    var good_sells_record_description_list_str = good_id + ' good-sells-record-description-list-container';
    var good_sells_record_description_list = document.getElementsByClassName(good_sells_record_description_list_str)[0];
    var good_sells_record_description_list_length = parseInt(good_sells_record_description_list.getAttribute('list_length'), 10);
    if(good_sells_record_description_list_length > 1) {
        good_sells_record_description_list.setAttribute('list_length', (good_sells_record_description_list_length - 1));

        // record the text of the description list
        var good_sells_record_description_element_list_str = good_id + ' good-sells-record-description';
        var good_sells_record_description_element_list = document.getElementsByClassName(good_sells_record_description_element_list_str);
        var good_sells_record_description_text_list = [];
        for(var i = 0; i < good_sells_record_description_element_list.length; i++) {
            good_sells_record_description_text_list.push(good_sells_record_description_element_list[i].value);
        }

        var good_sells_record_description_value = document.getElementsByClassName(good_id + ' description_' + good_sells_record_description_list_length + ' good-sells-record-description')[0].value;
            
        var delete_description_html = good_sells_record_description_html_code_generator(good_id, good_sells_record_description_list_length, good_sells_record_description_value);
        good_sells_record_description_list.innerHTML = good_sells_record_description_list.innerHTML.replace(delete_description_html, '');

        // recover the text of the description list
        for(var i = 0; i < good_sells_record_description_element_list.length; i++) {
            good_sells_record_description_element_list[i].value = good_sells_record_description_text_list[i];
        }
    }
}

// add event listener to good sells record delete button
function good_sells_record_delete_button_onclick(good_id) {
    // console.log("good_sells_record_delete_button_onclick()");
    
    var good_sells_record_classification_str = good_id + ' good-sells-record-classification';
    var good_sells_record_classification = document.getElementsByClassName(good_sells_record_classification_str)[0].value;
    var classificationGoodListRef = firebase.database().ref('good_list').child(good_sells_record_classification);
    classificationGoodListRef.once('value').then(function(snapshot) {
        snapshot.forEach(function(childsnapshot) {
            if(childsnapshot.val().good_seller == userEmail) {
                /*
                var good_id = childsnapshot.val().good_id;
                var good_image_url = childsnapshot.val().good_image_url;
                var good_name = childsnapshot.val().good_name;
                var good_price = childsnapshot.val().good_price;
                var good_num = childsnapshot.val().good_num;
                var good_classification = snapshot.key;
                var good_seller = childsnapshot.val().good_seller;
                var good_description_list = childsnapshot.val().good_description_list;
                var good_buyer_list = childsnapshot.val().good_buyer_list;
                var good_sells_record = good_sells_record_html_code_generator(good_id, good_image_url, good_name, good_price, good_num, good_classification, good_seller, good_description_list, good_buyer_list);

                var good_sells_record_list = document.getElementById('good-sells-record-list');
                good_sells_record_list.innerHTML.replace(good_sells_record, '');
                */

                var childsnapshotRef = childsnapshot.ref;
                childsnapshotRef.remove();
            
                // remove good_id from good_serial_number_list
                var goodSerialNumberListRef = firebase.database().ref('good_serial_number_list');
                goodSerialNumberListRef.once('value').then(function(snapshot) {
                    snapshot.forEach(function(childsnapshot) {
                        if(childsnapshot.val().good_serial_number == good_id) {
                            var childsnapshotRef = childsnapshot.ref;
                            childsnapshotRef.remove();
                        }
                    });
                }).catch(function(error) {
                    var errorCode= error.code;
                    var errorMessage= error.message;
                    create_custom_alert('error', errorMessage);
                });
            
                create_custom_alert('success', 'Successfully delete your selling good information from the server. Automatically reloading the current page.');
                setTimeout(function(){
                    window.location.reload();
                }, 1000);
            }
        });
    });
}

// add event listener to good sells record confirm button
function good_sells_record_confirm_button_onclick(good_id) {
    // console.log("good_sells_record_confirm_button_onclick()");
    
    var good_sells_record_classification_str = good_id + ' good-sells-record-classification';
    var good_sells_record_classification = document.getElementsByClassName(good_sells_record_classification_str)[0].value;
    var classificationGoodListRef = firebase.database().ref('good_list').child(good_sells_record_classification);
    classificationGoodListRef.once('value').then(function(snapshot) {
        snapshot.forEach(function(childsnapshot) {
            if(childsnapshot.val().good_seller == userEmail) {
                var new_good_sells_record_image_url = document.getElementsByClassName(good_id + ' good-sells-record-image')[0].src;
                var new_good_sells_record_name = document.getElementsByClassName(good_id + ' good-sells-record-name')[0].value;
                var new_good_sells_record_price = document.getElementsByClassName(good_id + ' good-sells-record-price')[0].value;
                var new_good_sells_record_num = document.getElementsByClassName(good_id + ' good-sells-record-num')[0].value;
                var new_good_sells_record_description_list = [];
                var new_good_sells_record_description_element_list = document.getElementsByClassName(good_id + ' good-sells-record-description');
                for(var i = 0; i < new_good_sells_record_description_element_list.length; i++) {
                    new_good_sells_record_description_list.push(new_good_sells_record_description_element_list[i].value);
                }
            
                if(new_good_sells_record_image_url == '' || new_good_sells_record_name == '' || new_good_sells_record_price == '' || new_good_sells_record_num == '') {
                    var good_sells_record_name = childsnapshot.val().good_name;
                    create_custom_alert('error', 'Please complete all required columns of the good "' + good_sells_record_name + '".');
                }
                else {
                    childsnapshot.ref.update({
                        good_image_url: new_good_sells_record_image_url,
                        good_name: new_good_sells_record_name,
                        good_price: new_good_sells_record_price,
                        good_num: new_good_sells_record_num,
                        good_description_list: new_good_sells_record_description_list
                    });
            
                    create_custom_alert('success', 'Successfully update your selling good information to the server. Automatically reloading the current page.');
                    setTimeout(function(){
                        window.location.reload();
                    }, 1000);
                }
              }
        });
    });
}

// new good sells description html code generator
function new_good_sells_description_html_code_generator(description_id) {
    var str_1 = '<div class="new-good-sells-description-container">' + 
                  '<input type="text" class="description_'; /*description_id*/
    var str_2 =                                                               ' new-good-sells-description form-control" placeholder="Good\'s Description" required="required">' + 
                '</div>';

    return str_1 + description_id + str_2;
}

// good sells record html code generator
function good_sells_record_html_code_generator(good_id, good_image_url, good_name, good_price, good_num, good_classification, good_seller, good_description_list, good_buyer_list) {
    var good_description_list_html = good_sells_record_description_list_html_code_generator(good_id, good_description_list);
    var good_buyer_list_html = good_sells_record_buyer_list_html_code_generator(good_id ,good_buyer_list);
    
    var str_1 = '<div class="good-sells-record">' + 
                  '<div class="good-sells-record-title-container">' + 
                    '<span class="good-sells-record-title">Good Sells Record</span>' + 
                  '</div>' + 
                '<div class="good-sells-record-top">' + 
                  '<div class="good-sells-record-top-left">' + 
                    '<form>' + 
                      '<div class="good-sells-record-image-title-container input-group-prepend">' + 
                        '<span class="good-sells-record-image-title prepend-text input-group-text">' + 
                          '<span>Good\'s Image</span>' + 
                          '<span class="good-sells-record-information-description">*</span>' + 
                        '</span>' + 
                      '</div>' + 
                    '</form>' + 
                    '<form>' + 
                      '<div class="good-sells-record-image-container">' + 
                        '<img class="' + good_id + ' good-sells-record-image" src="'; /*good_image_url*/
    var str_2 =                                                                                         '">' + 
                      '</div>' + 
                    '</form>' + 
                    '<div class="good-sells-record-image-btn-container">' + 
                      '<button onclick="good_sells_record_image_button_onclick(\'' + good_id + '\')" class="' + good_id + ' good-sells-record-image-btn btn btn-outline-secondary btn-block">Upload</button>' + 
                      '<input onchange="good_sells_record_image_file_reader_onchange(\'' + good_id + '\')" type="file" accept="image/*" class="' + good_id + ' good-sells-record-image-file-reader" required="required">' + 
                    '</div>' + 
                  '</div>' + 
                  '<div class="good-sells-record-top-right">' + 
                    '<form>' + 
                      '<div class="good-sells-record-information-title-container input-group-prepend">' + 
                        '<span class="good-sells-record-information-title prepend-text input-group-text">Good\'s Information</span>' + 
                      '</div>' + 
                    '</form>' + 
                    '<div class="good-sells-record-information-container">' + 
                      '<form>' + 
                        '<div class="input-group mb-2">' + 
                          '<div class="input-group-prepend">' + 
                            '<span class="prepend-text input-group-text">' + 
                              '<span>Name</span>' + 
                              '<span class="good-sells-record-information-description">*</span>' + 
                            '</span>' + 
                          '</div>' + 
                          '<input type="text" class="' + good_id + ' good-sells-record-name form-control" placeholder="Good\'s Name" value="'; /*good_name*/
    var str_3 =                                                                                                                                             '" required="required">' + 
                        '</div>' + 
                      '</form>' + 
                      '<form>' + 
                        '<div class="input-group mb-2">' + 
                          '<div class="input-group-prepend">' + 
                            '<span class="prepend-text input-group-text">' + 
                              '<span>Price</span>' + 
                              '<span class="good-sells-record-information-description">*</span>' + 
                            '</span>' + 
                          '</div>' + 
                          '<input type="number" min="1" class="' + good_id + ' good-sells-record-price form-control" placeholder="Good\'s Price" value="'; /*good_price*/
    var str_4 =                                                                                                                                                         '" required="required">' + 
                        '</div>' + 
                      '</form>' + 
                      '<form>' + 
                        '<div class="input-group mb-2">' + 
                          '<div class="input-group-prepend">' + 
                            '<span class="prepend-text input-group-text">' + 
                              '<span>Quantity</span>' + 
                              '<span class="good-sells-record-information-description">*</span>' + 
                            '</span>' + 
                          '</div>' + 
                          '<input type="number" min="1" class="' + good_id + ' good-sells-record-num form-control" placeholder="Good\'s Quantity" value="'; /*good_num*/
    var str_5 =                                                                                                                                                        '" required="required">' + 
                        '</div>' + 
                      '</form>' + 
                      '<form>' + 
                        '<div class="input-group mb-2">' + 
                          '<div class="input-group-prepend">' + 
                            '<span class="prepend-text input-group-text">' + 
                              '<span>Classification</span>' + 
                            '</span>' + 
                          '</div>' + 
                          '<input type="text" class="' + good_id + ' good-sells-record-classification form-control" placeholder="Good\'s Classification" value="'; /*good_classification*/
    var str_6 =                                                                                                                                                                          '" required="required" readonly="readonly">' + 
                        '</div>' + 
                      '</form>' + 
                      '<form>' + 
                        '<div class="input-group mb-2">' + 
                          '<div class="input-group-prepend">' + 
                            '<span class="prepend-text input-group-text">Seller</span>' + 
                          '</div>' + 
                          '<input type="text" class="' + good_id + ' good-seller form-control" placeholder="Good\'s Seller" value="'; /*good_seller*/
    var str_7 =                                                                                                                                     '" readonly="readonly">' + 
                        '</div>' + 
                      '</form>' + 
                    '</div>' + 
                  '</div>' + 
                '</div>' + 
                '<div class="good-sells-record-down">' + 
                '<div class="good-sells-record-description-area-container">' + 
                  '<form>' + 
                    '<div class="good-sells-record-description-title-container input-group-prepend">' + 
                      '<span class="good-sells-record-description-title prepend-text input-group-text">Good\'s Description</span>' + 
                    '</div>' + 
                  '</form>' + 
                  '<div class="good-sells-record-description-information-container">' + 
                    '<form>';
                      /*good_description_list*/
    var str_8 =     '</form>' + 
                    '<div class="good-sells-record-description-btn-container">' + 
                      '<div class="good-sells-record-description-btn">' + 
                        '<div class="good-sells-record-description-add-btn-container">' + 
                          '<button onclick="good_sells_record_description_add_button_onclick(\'' + good_id + '\')" type="button" class="' + good_id + ' good-sells-record-description-add-btn btn btn-outline-danger btn-block">+</button>' + 
                        '</div>' + 
                        '<div class="good-sells-record-description-delete-btn-container">' + 
                          '<button onclick="good_sells_record_description_delete_button_onclick(\'' + good_id + '\')" type="button" class="' + good_id + ' good-sells-record-description-delete-btn btn btn-outline-danger btn-block">-</button>' + 
                        '</div>' + 
                      '</div>' + 
                    '</div>' + 
                  '</div>' + 
                '</div>' + 
                '<div class="good-sells-record-buyer-area-container">' + 
                  '<form>' + 
                    '<div class="good-sells-record-buyer-title-container input-group-prepend">' + 
                      '<span class="good-sells-record-buyer-title prepend-text input-group-text">Good\'s Buyer</span>' + 
                    '</div>' + 
                  '</form>' + 
                  '<div class="good-sells-record-buyer-information-container">' + 
                    '<form>';
                      /*good_buyer_list*/
    var str_9 =     '</form>' + 
                  '</div>' + 
                '</div>' + 
              '</div>' + 
              '<div class="good-sells-record-btn-container">' + 
                '<div class="good-sells-record-btn">' + 
                  '<span class="good-sells-record-delete">' + 
                    '<div class="good-sells-record-delete-btn-container">' + 
                      '<button onclick="good_sells_record_delete_button_onclick(\'' + good_id + '\')" type="button" class="' + good_id + ' good-sells-record-delete-btn btn btn-outline-danger btn-block">Delete</button>' + 
                    '</div>' + 
                  '</span>' + 
                  '<span class="good-sells-record-confirm">' + 
                    '<div class="good-sells-record-confirm-btn-container">' + 
                      '<button onclick="good_sells_record_confirm_button_onclick(\'' + good_id + '\')" type="button" class="' + good_id + ' good-sells-record-confirm-btn btn btn-outline-success btn-block">Confirm</button>' + 
                    '</div>' + 
                  '</span>' + 
                '</div>' + 
              '</div>' + 
            '</div>';
    
    return str_1 + good_image_url + str_2 + good_name + str_3 + good_price + str_4 + good_num + str_5 + good_classification + str_6 + good_seller + str_7 + good_description_list_html + str_8 + good_buyer_list_html + str_9;
}

// good sells record html code generator
function good_sells_record_description_list_html_code_generator(good_id, good_description_list) {
    var good_sells_record_description_html_code_list = [];
    if(good_description_list != undefined) {
      for(var i = 0; i < good_description_list.length; i++) {
          var good_sells_record_description_html = good_sells_record_description_html_code_generator(good_id, (i+1), good_description_list[i]);
          good_sells_record_description_html_code_list.push(good_sells_record_description_html);
      }
    }

    var str_1 = '<div class="' + good_id + ' good-sells-record-description-list-container" list_length="'; /*good_description_list.length*/
    var str_2 =                                                                                                                           '">'; 
                  /*good_sells_record_description_html_code_list.join('')*/
    var str_3 = '</div>';

    return str_1 + good_description_list.length + str_2 + good_sells_record_description_html_code_list.join('') + str_3;
}

// good sells record description html code generator
function good_sells_record_description_html_code_generator(good_id, description_id, description_value) {
    var str_1 = '<div class="' + good_id + ' good-sells-record-description-container">' + 
                  '<input type="text" class="'; /*good_id*/
    var str_2 =                                            ' description_'; /*description_id*/
    var str_3 =                                                                               ' good-sells-record-description form-control" placeholder="Good\'s Description" value="'; /*description_value*/
    var str_4 =                                                                                                                                                                                              '" required="required">' + 
                '</div>';
    
    return str_1 + good_id + str_2 + description_id + str_3 + description_value + str_4;
}

// good sells record buyer list html code generator
function good_sells_record_buyer_list_html_code_generator(good_id ,good_buyer_list) {
    var good_sells_record_buyer_html_code_list = [];
    if(good_buyer_list != undefined) {
        for(var i = 0; i < good_buyer_list.length; i++) {
            var good_sells_record_buyer_html = good_sells_record_buyer_html_code_generator(good_id, good_buyer_list[i]);
            good_sells_record_buyer_html_code_list.push(good_sells_record_buyer_html);
        }
    }

    var str_1 = '<div class="' + good_id + ' good-sells-record-buyer-list-container">';
                  /*good_sells_record_buyer_html_code_list.join('')*/
    var str_2 = '</div>';

    return str_1 + good_sells_record_buyer_html_code_list.join('') + str_2;
}

// good sells record buyer html code generator
function good_sells_record_buyer_html_code_generator(good_id, good_buyer) {
    var good_buyer_email = good_buyer.email;
    var good_buyer_good_num = good_buyer.good_num;

    var str_1 = '<div class="' + good_id + ' good-sells-record-buyer-container">' + 
                  '<input type="text" class="'; /*good_id*/
    var str_2 =                                            ' good-sells-record-buyer form-control" placeholder="Good\'s Buyer" value="'; /*good_buyer*/
    var str_3 =                                                                                                                                        '" required="required" readonly="readonly">' + 
                '</div>';

    return str_1 + good_id + str_2 + good_buyer_email + ' x ' + good_buyer_good_num + str_3;
}

// custom alert
function create_custom_alert(type, message) {
    var custom_alert_area = document.getElementById('custom-alert');
    if (type == "success") {
        str_html = "<div class='alert alert-success alert-dismissible fade show' role='alert'><strong>Success! </strong>" + message + "<button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button></div>";
        custom_alert_area.innerHTML = str_html;
    } else if (type == "error") {
        str_html = "<div class='alert alert-danger alert-dismissible fade show' role='alert'><strong>Error! </strong>" + message + "<button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button></div>";
        custom_alert_area.innerHTML = str_html;
    }
}

// authorization alert area
function create_authorization_alert(type, message) {
    var authorization_alert_area = document.getElementById('authorization-alert');
    if (type == "success") {
        str_html = "<div class='alert alert-success alert-dismissible fade show' role='alert'><strong>Authorization Agreed! </strong>" + message + "<button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button></div>";
        authorization_alert_area.innerHTML = str_html;
    } else if (type == "error") {
        str_html = "<div class='alert alert-danger alert-dismissible fade show' role='alert'><strong>Authorization Denied! </strong>" + message + "<button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button></div>";
        authorization_alert_area.innerHTML = str_html;
    }
}