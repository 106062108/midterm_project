const payment_serial_number_length = 20;
const payment_serial_number_random_base = 36;
const random_number_combination_upper_limit = Math.pow(payment_serial_number_random_base, payment_serial_number_length);

var shopping_cart_good_list_num = 0;
var shopping_cart_good_list_total_price = 0;

var payment_serial_number_list = [];
var payment_serial_number = undefined;

var userEmail = undefined;

window.onload = function() {
    init();
}

window.onresize = function() {
    good_image_resize();
}

// load payment serial number list
function load_payment_serial_number_list() {
    return newPromise = new Promise((resolve, reject) => {
        firebase.database().ref('payment_serial_number_list').once('value').then(function(snapshot) {
            snapshot.forEach(function(childsnapshot) {
                payment_serial_number_list.push(childsnapshot.val().payment_serial_number);
            });
            // console.log("load_payment_serial_number_list() success");
            resolve('success');
        }).catch(function(error) {
            // console.log(error);
            reject('fail');
        });
    });
}

// test new payment serial number
function test_new_payment_serial_number(new_random_serial_number) {
    return new Promise((resolve, reject) => {
        var random_number_combination_counter = 0;
        var serial_number_existed;
        do {
            serial_number_existed = false;
            if(payment_serial_number_list.length == 0) {
                // console.log("test_new_payment_serial_number() success");
                resolve('success');
            }
            else {
                for(var i = 0; i < payment_serial_number_list.length; i++) {
                    if(new_random_serial_number == payment_serial_number_list[i]) {
                        serial_number_existed = true;
                        new_random_serial_number = Math.random().toString(payment_serial_number_random_base).substring(2, (2+payment_serial_number_length));
                    }
    
                    if(i == payment_serial_number_list.length-1) {
                        if(serial_number_existed == true) {
                            reject('fail');
                        }
                        else {
                            // console.log("test_new_payment_serial_number() success");
                            resolve('success');
                        }
                    }
                }
            }

            random_number_combination_counter++;
            if(random_number_combination_counter == random_number_combination_upper_limit) {
                // console.log("random serial number exhausted");
                reject('fail');
            }
        } while((serial_number_existed == true) && (random_number_combination_counter < random_number_combination_upper_limit));
    });
}

// payment serial number generator
async function payment_serial_number_generator() {
    try {
        var load_payment_serial_number_list_promise = await load_payment_serial_number_list();
        if(load_payment_serial_number_list_promise == 'success') {
            var new_random_serial_number = Math.random().toString(payment_serial_number_random_base).substring(2, (2+payment_serial_number_length));
            try {
                var test_new_payment_serial_number_promise = await test_new_payment_serial_number(new_random_serial_number);
                if(test_new_payment_serial_number_promise == 'success') {
                    // console.log("payment_serial_number_generator() success : " + new_random_serial_number);
                    payment_serial_number = new_random_serial_number;
                }
            }
            catch(error) {
                payment_serial_number = undefined;
                // console.log(error);
            }
        }
    }
    catch(error) {
        payment_serial_number = undefined;
        // console.log(error);
    }
}

// good image resize
function good_image_resize() {
    var good_image_container = document.getElementsByClassName('good-image-container');
    for(var i = 0; i < good_image_container.length; i++) {
        var processing_good_image_container = good_image_container[i];
        processing_good_image_container.style.height = processing_good_image_container.clientWidth + 'px';
    }
}

// init
function init() {
    var signout_btn_clicked = false;
    // check user's sign state
    firebase.auth().onAuthStateChanged(function (user) {
        var account = document.getElementById('side-bar-account');
        var shopping_cart = document.getElementById('side-bar-shopping-cart');
        if(user) {
            // if user is in signin state
            userEmail = user.email;
            account.innerHTML = '<a class="nav-link dropdown-toggle" data-toggle="dropdown">' + userEmail + '</a>' + 
                                '<div id="side-bar-account-menu" class="dropdown-menu">' + 
                                  '<a id="homepage-btn" class="dropdown-item">Home</a>' + 
                                  '<a id="dashboard-btn" class="dropdown-item">Dashboard</a>' + 
                                  '<a id="signout-btn" class="dropdown-item">Signout</a>' + 
                                '</div>';
            shopping_cart.style.visibility = 'visible';

            // add event listener to homepage button
            var homepage_btn = document.getElementById('homepage-btn');
            homepage_btn.addEventListener('click', function() {
                window.location.reload();
            });
            
            // add event listener to dashboard button
            var dashboard_btn = document.getElementById('dashboard-btn');
            dashboard_btn.addEventListener('click', function() {
                window.location.assign("dashboard.html");
            });

            // add event listener to signout button
            var signout_btn = document.getElementById('signout-btn');
            signout_btn.addEventListener('click', function() {
                firebase.auth().signOut().then(function() {
                    signout_btn_clicked = true;
                    create_custom_alert('success', 'Successfully signout. Automatically redirecting to the Signin Page.')
                    setTimeout(function() {
                        window.location.assign("signin.html");
                    }, 1000);
                }).catch(function(error) {
                    var errorCode= error.code;
                    var errorMessage= error.message;
                    create_custom_alert('error', errorMessage);
                });
            });

            // create a random payment serial number that each character is selected from {0~9, a~z}
            payment_serial_number_generator();

            // load good list
            var goodListRef = firebase.database().ref('good_list');
            var goodListRef_list = [];
            var breadGoodListRef = goodListRef.child('bread');
            goodListRef_list.push(breadGoodListRef);
            var cakeGoodListRef = goodListRef.child('cake');
            goodListRef_list.push(cakeGoodListRef);
            var snackGoodListRef = goodListRef.child('snack');
            goodListRef_list.push(snackGoodListRef);
            var beverageGoodListRef = goodListRef.child('beverage');
            goodListRef_list.push(beverageGoodListRef);
            var otherGoodListRef = goodListRef.child('other');
            goodListRef_list.push(otherGoodListRef);

            load_good_list(goodListRef_list);
        } else {
            // if user is in signout state
            account.innerHTML = '<a class="nav-link dropdown-toggle" data-toggle="dropdown">Guest</a>' + 
                                '<div id="side-bar-account-menu" class="dropdown-menu">' + 
                                  '<a id="homepage-btn" class="dropdown-item" href="index.html">Home</a>' + 
                                  '<a id="signin-btn" class="dropdown-item" href="signin.html">Signin</a>' + 
                                '</div>';
            shopping_cart.style.visibility = 'hidden';

            if(signout_btn_clicked != true) {
                // load good list
                var goodListRef = firebase.database().ref('good_list');
                var goodListRef_list = [];
                var breadGoodListRef = goodListRef.child('bread');
                goodListRef_list.push(breadGoodListRef);
                var cakeGoodListRef = goodListRef.child('cake');
                goodListRef_list.push(cakeGoodListRef);
                var snackGoodListRef = goodListRef.child('snack');
                goodListRef_list.push(snackGoodListRef);
                var beverageGoodListRef = goodListRef.child('beverage');
                goodListRef_list.push(beverageGoodListRef);
                var otherGoodListRef = goodListRef.child('other');
                goodListRef_list.push(otherGoodListRef);

                load_good_list(goodListRef_list);
            }
        }
    });

    // add event listener to good list side bar button
    var good_list_side_bar_clicked_button = document.getElementsByClassName('good-list-side-bar-btn active')[0];
    var good_list_showed = document.getElementsByClassName('child-good-list active')[0];
    var good_list_side_bar_button = document.getElementsByClassName('good-list-side-bar-btn');
    for(var i = 0; i < good_list_side_bar_button.length; i++) {
        good_list_side_bar_button[i].addEventListener('click', function() {
            if(!this.classList.contains('active')) {
                // change the active button in the good list side bar
                good_list_side_bar_clicked_button.classList.remove('active');
                this.classList.add('active');
                good_list_side_bar_clicked_button = this;

                // change the good list according to the current active button
                var clicked_button_id = good_list_side_bar_clicked_button.className.split(' ')[0];
                var new_good_list_showed_str = clicked_button_id + ' child-good-list';
                var new_good_list_showed_element = document.getElementsByClassName(new_good_list_showed_str)[0];
                good_list_showed.style.display = 'none';
                new_good_list_showed_element.style.display = 'block';
                good_list_showed = new_good_list_showed_element;

                // change the good image size for small window width
                good_image_resize();
            }
        });
    }

    // add event listener to shopping cart menu checkout button
    var shopping_cart_menu_checkout_button = document.getElementById('shopping-cart-menu-checkout-btn');
    shopping_cart_menu_checkout_button.addEventListener('click', function() {
        // check user's sign state
        firebase.auth().onAuthStateChanged(function (user) {
            if(user) {
                // if user is in signin state
                var shopping_cart_menu_good_name_list = document.getElementsByClassName('shopping-cart-menu-good-name');
                var shopping_cart_menu_good_num_list = document.getElementsByClassName('shopping-cart-menu-good-num');
                var shopping_cart_menu_good_price_list = document.getElementsByClassName('shopping-cart-menu-good-price');
                var shopping_cart_list_length = shopping_cart_menu_good_name_list.length;

                // create shopping cart list containing goods which are in {good_name, good_price, good_price} format
                var shopping_cart_list = [];
                var good_id_list = [];
                var good_num_list = [];
                var good_classification_list = [];
                for(var i = 0; i < shopping_cart_list_length; i++) {
                    var good_id = shopping_cart_menu_good_name_list[i].className.split(' ')[0];
                    var good_name = shopping_cart_menu_good_name_list[i].innerHTML;
                    var good_num = parseInt(shopping_cart_menu_good_num_list[i].innerHTML, 10);
                    var good_price = parseInt(shopping_cart_menu_good_price_list[i].innerHTML, 10);
                    var good_classification = document.getElementsByClassName(good_id + ' good-classification')[0].innerHTML;
                    
                    var shopping_cart_good = {
                        good_name: good_name,
                        good_num: good_num,
                        good_price: good_price
                    }
                    shopping_cart_list.push(shopping_cart_good);

                    good_id_list.push(good_id);
                    good_num_list.push(good_num);
                    good_classification_list.push(good_classification);
                }

                create_custom_alert('success', 'The page is processing your shopping cart list to the server. Please wait for a while.');
                update_good_list(good_id_list, good_num_list, good_classification_list).then(function() {
                    if(payment_serial_number == undefined) {
                        create_custom_alert('error', 'Server is busy now. Please try it again later.')
                    }
                    else {
                        var newUserShoppingCartListRef = firebase.database().ref('user_shopping_cart_list').push();
                        newUserShoppingCartListRef.set({
                            email: userEmail,
                            shopping_cart_good_list: shopping_cart_list,
                            shopping_cart_total_price: shopping_cart_good_list_total_price,
                            shopping_cart_payment_serial_number: payment_serial_number
                        });
                        
                        var newPaymentSerialNumberRef = firebase.database().ref('payment_serial_number_list').push();
                        newPaymentSerialNumberRef.set({
                            payment_serial_number: payment_serial_number
                        });
                        
                        create_custom_alert('success', 'Successfully deliver your shopping cart list to the server. Automatically redirecting to the Payment Page.')
                        setTimeout(function() {
                            window.location.assign("payment.html");
                        }, 1000);
                    }
                }).catch(function(error) {
                    // console.log(error);
                });
            } else {
                // if user is in signout state
                create_authorization_alert('error', 'You should signin first to use the shopping cart.')
            }
        });
    });
}

// load good list
async function load_good_list(goodListRef_list) {
    var child_good_list = document.getElementsByClassName('child-good-list');
    for(var i = 0; i < child_good_list.length; i++) {
        child_good_list[i].style.visibility = 'hidden';
    }
    
    var goodListRef_promise_list = [];
    for(var i = 0; i < goodListRef_list.length; i++) {
        goodListRef_promise_list.push(await load_classification_good_list(goodListRef_list[i]));
    }

    Promise.all(goodListRef_promise_list).then(values => {
        // add event listener to good cart button
        var good_cart_buttons = document.getElementsByClassName('good-cart-btn');
        for(var i = 0; i < good_cart_buttons.length; i++) {
            good_cart_buttons[i].addEventListener('click' , function() {
                var processing_button = this;
                firebase.auth().onAuthStateChanged(function (user) {
                    if(user) {
                        // if user is in signin state
                        var good_id = processing_button.className.split(' ')[0];
                        var shopping_cart_menu_good_num_element = document.getElementsByClassName(good_id + ' shopping-cart-menu-good-num')[0];
                        var shopping_cart_menu_good_num;
                        if(shopping_cart_menu_good_num_element == undefined) {
                            shopping_cart_menu_good_num = 0;
                        }
                        else {
                            shopping_cart_menu_good_num = parseInt(shopping_cart_menu_good_num_element.innerHTML);
                        }
                        var good_cart_num = parseInt(document.getElementsByClassName(good_id + ' good-cart-num')[0].value);
                        var good_storage = parseInt(document.getElementsByClassName(good_id + ' good-storage')[0].innerHTML);

                        if(shopping_cart_menu_good_num + good_cart_num > good_storage) {
                            var good_name = document.getElementsByClassName(good_id + ' good-name')[0].innerHTML;
                            if(good_cart_num > good_storage) {
                                create_custom_alert('error', 'The selected quantity of the ' + good_name + ' is out of limits. Please check and select it again.');
                            }
                            else {
                                create_custom_alert('error', 'You have added all of the ' + good_name + ' in your shopping cart.');
                            }
                        }
                        else {
                            shopping_cart_good_list_num++;
                                   
                        // get selected good id
                        var selected_good_id = processing_button.className.split(' ')[0];
                        // console.log(selected_good_id);
        
                        // get selected good name
                        var selected_good_name_str = selected_good_id + ' good-name';
                        var selected_good_name_element = document.getElementsByClassName(selected_good_name_str)[0];
                        var selected_good_name = selected_good_name_element.innerHTML;
                        // console.log(selected_good_name);
        
                        // get selected good num
                        var selected_good_num_str = selected_good_id + ' good-cart-num';
                        var selected_good_num_element = document.getElementsByClassName(selected_good_num_str)[0];
                        var selected_good_num = parseInt(selected_good_num_element.value, 10);
                        // console.log(selected_good_num);
        
                        // get selected good num limit
                        var selected_good_min_num = selected_good_num_element.min;
                        var selected_good_max_num = selected_good_num_element.max;
                        // console.log(selected_good_min_num, selected_good_max_num);
        
                        // get selected good price
                        var selected_good_price_str = selected_good_id + ' good-price';
                        var selected_good_price_element = document.getElementsByClassName(selected_good_price_str)[0];
                        var selected_good_price = parseInt(selected_good_price_element.innerHTML, 10);
                        // console.log(selected_good_price);
        
                        if(selected_good_num < selected_good_min_num || selected_good_num > selected_good_max_num) {
                            create_custom_alert('error', 'The selected quantity of the ' + selected_good_name + ' is out of limits. Please check and select it again.');
                        }
                        else {
                            // update shopping cart menu html code
                            var shopping_good_list = document.getElementById('shopping-cart-good-list-container');
                            var good_has_existed_in_shopping_cart = search_good_in_shopping_cart(selected_good_id);
                        if(good_has_existed_in_shopping_cart) {
                                // get existed good num in the shopping cart
                                var old_good_num_str = selected_good_id + ' shopping-cart-menu-good-num';
                                var old_good_num_element = document.getElementsByClassName(old_good_num_str)[0];
                                var old_good_num = parseInt(old_good_num_element.innerHTML, 10);
        
                                // get existed good price in the shopping cart
                                var old_good_price_str = selected_good_id + ' shopping-cart-menu-good-price';
                                var old_good_price_element = document.getElementsByClassName(old_good_price_str)[0];
                            
                                // update good num & good price in shopping cart
                                var new_good_num = old_good_num + selected_good_num;
                                var new_good_price = selected_good_price * new_good_num;
                                old_good_num_element.innerHTML = new_good_num;
                                old_good_price_element.innerHTML = new_good_price;
        
                                // update shopping cart menu total price
                                var shopping_cart_checkout_total_price = document.getElementById('shopping-cart-checkout-total-price');
                                shopping_cart_good_list_total_price += selected_good_price * selected_good_num;
                                shopping_cart_checkout_total_price.innerHTML = shopping_cart_good_list_total_price;
                            }
                            else {
                                var shopping_good_list_good_html = shopping_good_list_good_html_generator(selected_good_id, selected_good_name, selected_good_num, selected_good_price*selected_good_num);
                                shopping_good_list.innerHTML += shopping_good_list_good_html;
        
                                // update shopping cart menu total price
                                var shopping_cart_checkout_total_price = document.getElementById('shopping-cart-checkout-total-price');
                                shopping_cart_good_list_total_price += selected_good_price * selected_good_num;
                                shopping_cart_checkout_total_price.innerHTML = shopping_cart_good_list_total_price;
                            }
        
                            var no_goods_in_cart = document.getElementById('no-goods-in-cart-container');
                            var shopping_cart_checkout = document.getElementById('shopping-cart-checkout-container');
                            if(shopping_cart_good_list_num == 0) {
                                no_goods_in_cart.style.display = 'block';
                                shopping_cart_checkout.style.display = 'none';
                            }
                            else {
                                no_goods_in_cart.style.display = 'none';
                                shopping_cart_checkout.style.display = 'block';
                            }
        
                            create_custom_alert('success', 'Successfully add ' + selected_good_name + ' x ' + selected_good_num + ' into your shopping cart.');
                        }    
                        }
                    } else {
                        // if user is in signout state
                        create_authorization_alert('error', 'You should signin first to use the shopping cart.')
                    }
                });
            });
        }
    });

    document.getElementById('good-list-loading-alert').style.display = 'none';
    for(var i = 0; i < child_good_list.length; i++) {
        child_good_list[i].style.visibility = 'visible';
    }
}

// load classification good list
function load_classification_good_list(classificationGoodListRef) {
    // load classification good list
    return new Promise((resolve, reject) => {
        classificationGoodListRef.once('value').then(function(snapshot) {
            snapshot.forEach(function(childsnapshot) {
                var good_id = childsnapshot.val().good_id;
                var good_image_url = childsnapshot.val().good_image_url;
                var good_name = childsnapshot.val().good_name;
                var good_description_list = childsnapshot.val().good_description_list;
                var good_storage = childsnapshot.val().good_num;
                var good_seller = childsnapshot.val().good_seller;
                var good_price = childsnapshot.val().good_price;
                var good_classification = snapshot.key;
                var good = good_list_good_html_code_generator(good_id, good_image_url, good_name, good_description_list, good_storage, good_seller, good_price, good_classification);
    
                // update child good list html code
                var child_good_list = document.getElementsByClassName(good_classification + ' child-good-list')[0];
                child_good_list.innerHTML += good;
            });
            resolve('success');
        }).catch(function(error) {
            var errorCode= error.code;
            var errorMessage= error.message;
            create_custom_alert('error', errorMessage);
            reject('fail');
        });
    });
}

// update good list in firebase database
async function update_good_list(good_id_list, good_num_list, good_classification_list) {
    var update_child_good_list_promise_list = [];
    for(var i = 0; i < good_classification_list.length; i++) {
        update_child_good_list_promise_list.push(await update_child_good_list(good_id_list[i], good_num_list[i], good_classification_list[i]));
    }

    return new Promise((resolve, reject) => {
        Promise.all(update_child_good_list_promise_list).then(function() {
            resolve('success');
        }).catch(function(error) {
            // console.log(error);
            reject('fail');
        });
    });
}

// update child good list in firebase database
function update_child_good_list(good_id, good_num, good_classification) {
    return new Promise((resolve, reject) => {
        var childGoodListRef = firebase.database().ref('good_list').child(good_classification).ref;
        childGoodListRef.once('value').then(function(snapshot) {
            snapshot.forEach(function(childsnapshot) {
                if(childsnapshot.val().good_id == good_id) {
                    var good_storage = parseInt(childsnapshot.val().good_num);
                    childsnapshot.ref.update({
                        good_num: (good_storage-good_num)
                    });

                    var good_buyer_list = childsnapshot.val().good_buyer_list;
                    var new_good_buyer_list = [];
                    if(good_buyer_list == undefined) {
                        new_good_buyer_list.push({
                            email: userEmail,
                            good_num: good_num
                        });
                        childsnapshot.ref.update({
                            good_buyer_list: new_good_buyer_list
                        });
                    }
                    else {
                        for(var i = 0; i < good_buyer_list.length; i++) {
                            new_good_buyer_list.push(good_buyer_list[0]);
                        }
                        new_good_buyer_list.push({
                            email: userEmail,
                            good_num: good_num
                        });
                        childsnapshot.ref.update({
                            good_buyer_list: new_good_buyer_list
                        });
                    }
                }
            });

            resolve('success');
        }).catch(function(error) {
            var errorCode= error.code;
            var errorMessage= error.message;
            create_custom_alert('error', errorMessage);

            reject('fail');
        });
    });
}

// shopping cart good list good html generator
function shopping_good_list_good_html_generator(good_id, good_name, good_num, good_price) {
   var str_1 = '<div class="' + good_id + ' shopping-cart-menu-good">' + 
                  '<span class="' + good_id + ' shopping-cart-menu-good-delete-btn-container">' + 
                    '<button onclick="good_delete(\'' + good_id + '\')" type="button" class="' + good_id + ' shopping-cart-menu-good-delete-btn btn btn-outline-primary btn-block">Delete</button>' + 
                  '</span>' + 
                  '<span class="' + good_id + ' shopping-cart-menu-good-container">' + 
                    '<div class="' + good_id + ' shopping-cart-menu-good-left">' + 
                      '<span class="' + good_id + ' shopping-cart-menu-good-name">'; /*good_name*/
    var str_2 =                                                                                     '</span>' + 
                      '<span class="' + good_id + ' shopping-cart-menu-good-description"> x </span>' + 
                      '<span class="' + good_id + ' shopping-cart-menu-good-num">'; /*good_num*/
    var str_3 =                                                                                   '</span>' + 
                    '</div>' + 
                    '<div class="' + good_id + ' shopping-cart-menu-good-right">' + 
                      '<span class="' + good_id + ' shopping-cart-menu-good-price">'; /*good_price*/
    var str_4 =                                                                                       '</span>' + 
                      '<span class="' + good_id + ' shopping-cart-menu-good-description"> 元</span>' + 
                    '</div>' + 
                  '</span>' + 
                '</div>';
    return str_1 + good_name + str_2 + good_num + str_3 + good_price + str_4;
}

// search for the ongoing added good which has existed in the shopping cart or not
function search_good_in_shopping_cart(good_id) {
    var shopping_good_list = document.getElementById('shopping-cart-good-list-container');
    var shopping_good_list_html = shopping_good_list.innerHTML;
    return (shopping_good_list_html.search(good_id) >= 0);
}

// good list good html code generator
function good_list_good_html_code_generator(good_id, good_image_url, good_name, good_description_list, good_storage, good_seller, good_price, good_classification) {
    var good_description_list_html = good_list_good_description_list_html_code_generator(good_description_list);

    var str_1 = '<div class="' + good_id + ' good-container">' + 
                  '<div class="' + good_id + ' good-image-container">' + 
                    '<img class="' + good_id + ' good-image" src="'; /*good_image_url*/
    var str_2 =                                                                       '" alt="'; /*good_name*/
    var str_3 =                                                                                               '">' + 
                  '</div>' + 
                  '<div class="' + good_id + ' good-information-container">' + 
                    '<div class="' + good_id + ' good-name">'; /*good_name*/
    var str_4 =                                                            '</div>';
                      /*good_description_list*/
    var str_5 =     '<div class="' + good_id + ' good-storage-and-price-container">' + 
                      '<div class="good-storage-container">' + 
                        '<span class="' + good_id + ' good-storage-description">庫存： </span> <span class="' + good_id + ' good-storage">';/*good_storage*/
    var str_6 =                                                                                                                                           '</span>' + 
                      '</div>' + 
                      '<div class="good-price-container">' + 
                        '<span class="' + good_id + ' good-price-description">售價： </span> <span class="' + good_id + ' good-price">';/*good_price*/
    var str_7 =                                                                                                                                     '</span> <span class="' + good_id + ' good-price-description"> 元</span>' + 
                      '</div>' + 
                    '</div>' + 
                    '<div class="' + good_id + ' good-cart-container">' + 
                      '<div class="input-group mb-3">' + 
                        '<div class="input-group-prepend">' + 
                          '<span class="prepend-text input-group-text">數量</span>' + 
                        '</div>' + 
                        '<input type="number" min="1" max="';/*good_storage*/
    var str_8;
    if(good_seller == userEmail) {
        str_8 =                                                              '" class="' + good_id + ' good-cart-num form-control" disabled="disabled">' + 
                      '</div>' + 
                      '<button type="button" class="' + good_id + ' good-cart-btn btn btn-secondary btn-block" disabled="disabled">你的商品</button>' + 
                    '</div>' + 
                    '<div class="' + good_id + ' good-classification">';/*good_classification*/
    }
    else {
        if(good_storage <= 0) {
            str_8 =                                                          '" class="' + good_id + ' good-cart-num form-control" disabled="disabled">' + 
                      '</div>' + 
                      '<button type="button" class="' + good_id + ' good-cart-btn btn btn-danger btn-block" disabled="disabled">售完</button>' + 
                    '</div>' + 
                    '<div class="' + good_id + ' good-classification">';/*good_classification*/
        }
        else {
            str_8 =                                                          '" class="' + good_id + ' good-cart-num form-control" value="1">' + 
                      '</div>' + 
                      '<button type="button" class="' + good_id + ' good-cart-btn btn btn-outline-primary btn-block">加入購物車</button>' + 
                    '</div>' + 
                    '<div class="' + good_id + ' good-classification">';/*good_classification*/
        }
    }
    
    var str_9 =                                                                               '</div>' + 
                  '</div>' + 
                '</div>';

    return str_1 + good_image_url + str_2 + good_name + str_3 + good_name + str_4 + good_description_list_html + str_5 + good_storage + str_6 + good_price + str_7 + good_storage + str_8 + good_classification + str_9;
}

// good list good description list html code generator
function good_list_good_description_list_html_code_generator(good_description_list) {
    var good_description_html_list = [];
    if(good_description_list != undefined) {
        for(var i = 0; i < good_description_list.length; i++) {
            good_description_html_list.push('<li>' + good_description_list[i] + '</li>');
        }
    }

    var str_1 = '<ul class="ice-cream-cake good-description">';
                  /*good_description_html_list.join('')*/
    var str_2 = '</ul>';

    return str_1 + good_description_html_list.join('') + str_2;
}

// add onclick to good delete button
function good_delete(selected_good_id) {
    shopping_cart_good_list_num--;
    
    // get selected good name
    var selected_good_name_str = selected_good_id + ' shopping-cart-menu-good-name';
    var selected_good_name_element = document.getElementsByClassName(selected_good_name_str)[0];
    var selected_good_name = selected_good_name_element.innerHTML;
    // console.log(selected_good_name);

    // get selected good num
    var selected_good_num_str = selected_good_id + ' shopping-cart-menu-good-num';
    var selected_good_num_element = document.getElementsByClassName(selected_good_num_str)[0];
    var selected_good_num = parseInt(selected_good_num_element.innerHTML, 10);
    // console.log(selected_good_num);

    // get selected good price
    var selected_good_price_str = selected_good_id + ' shopping-cart-menu-good-price';
    var selected_good_price_element = document.getElementsByClassName(selected_good_price_str)[0];
    var selected_good_price = parseInt(selected_good_price_element.innerHTML, 10);
    // console.log(selected_good_price);
    
    // update shopping cart menu html code
    var shopping_good_list = document.getElementById('shopping-cart-good-list-container');
    var shopping_good_list_good_html = shopping_good_list_good_html_generator(selected_good_id, selected_good_name, selected_good_num, selected_good_price); 
    shopping_good_list.innerHTML = shopping_good_list.innerHTML.replace(shopping_good_list_good_html, '');
    
    var no_goods_in_cart = document.getElementById('no-goods-in-cart-container');
    var shopping_cart_checkout = document.getElementById('shopping-cart-checkout-container');
    if(shopping_cart_good_list_num == 0) {
        no_goods_in_cart.style.display = 'block';
        shopping_cart_checkout.style.display = 'none';
    }
    else {
        no_goods_in_cart.style.display = 'none';
        shopping_cart_checkout.style.display = 'block';
    }

    var shopping_cart_checkout_total_price = document.getElementById('shopping-cart-checkout-total-price');
    shopping_cart_good_list_total_price -= selected_good_price;
    shopping_cart_checkout_total_price.innerHTML = shopping_cart_good_list_total_price;

    create_custom_alert('success', 'Successfully delete ' + selected_good_name + ' x ' + selected_good_num + ' from your shopping cart.');
}

// custom alert
function create_custom_alert(type, message) {
    var custom_alert_area = document.getElementById('custom-alert');
    if (type == "success") {
        str_html = "<div class='alert alert-success alert-dismissible fade show' role='alert'><strong>Success! </strong>" + message + "<button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button></div>";
        custom_alert_area.innerHTML = str_html;
    } else if (type == "error") {
        str_html = "<div class='alert alert-danger alert-dismissible fade show' role='alert'><strong>Error! </strong>" + message + "<button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button></div>";
        custom_alert_area.innerHTML = str_html;
    }
}

// authorization alert area
function create_authorization_alert(type, message) {
    var authorization_alert_area = document.getElementById('authorization-alert');
    if (type == "success") {
        str_html = "<div class='alert alert-success alert-dismissible fade show' role='alert'><strong>Authorization Agreed! </strong>" + message + "<button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button></div>";
        authorization_alert_area.innerHTML = str_html;
    } else if (type == "error") {
        str_html = "<div class='alert alert-danger alert-dismissible fade show' role='alert'><strong>Authorization Denied! </strong>" + message + "<button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button></div>";
        authorization_alert_area.innerHTML = str_html;
    }
}