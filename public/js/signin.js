window.onload = function() {
    init();
}

// init
function init() {
    var txtEmail = document.getElementById('inputEmail');
    var txtPassword = document.getElementById('inputPassword');
    var btnSignin = document.getElementById('btnSignin');
    var btnSigninWithGoogle = document.getElementById('btnSigninWithGoogle');
    var btnNewAccount = document.getElementById('btnNewAccount');

    if(!('Notification' in window)) {
        create_custom_alert('error', 'Your browser does not support chrome notification.');
    }

    var notifyConfig = {
        body: 'Notification Enables',
        tag: 'newArrival'
    };
    if(Notification.permission === 'default' || Notification.permission === 'undefined') {
        Notification.requestPermission(function (permission) {
            if(permission === 'granted') {
                var notification = new Notification('Notification', notifyConfig);
            }
        });
    }

    // signin
    btnSignin.addEventListener('click', function() {
        var userEmail = txtEmail.value;
        var userPassword = txtPassword.value;

        firebase.auth().signInWithEmailAndPassword(userEmail, userPassword).then(function() {
            create_custom_alert('success', 'Successfully signin. Automatically redirecting to the Home Page.');
            setTimeout(function() {
                window.location.assign("index.html");
            }, 1000);
        }).catch(function(error) {
            // Handle Errors here
            var errorCode= error.code;
            var errorMessage= error.message;
            create_custom_alert('error', errorMessage);
        });
    });

    // signin with google
    btnSigninWithGoogle.addEventListener('click', function () {
        var googleProvider = new firebase.auth.GoogleAuthProvider();

        firebase.auth().signInWithPopup(googleProvider).then(function(result) {
            if(result.credential) {
                // This gives you a Google Access Token. You can use it to access the Google API.
                var token = result.credential.accessToken;

                create_custom_alert('success', 'Successfully signin with Google. Automatically redirecting to the Home Page.');
                window.location.assign("index.html");
            }
            // The signed-in user info
            var user = result.user;
        }).catch(function(error) {
            // Handle Errors here
            var errorCode= error.code;
            var errorMessage= error.message;
            create_custom_alert('error', errorMessage);
        });
    });

    // new account
    btnNewAccount.addEventListener('click', function () {        
        var userEmail = txtEmail.value;
        var userPassword = txtPassword.value;

        firebase.auth().createUserWithEmailAndPassword(userEmail, userPassword).then(function() {
            create_custom_alert('success', 'Successfully create a new account. You can signin with your new account now.');
        }).catch(function(error) {
            // Handle Errors here.
            var errorCode = error.code;
            var errorMessage = error.message;
            
            create_custom_alert('error', errorMessage);
        });

        txtEmail.value = userEmail;
        txtPassword.value = '';
    });
}

// custom alert
function create_custom_alert(type, message) {
    var custom_alert_area = document.getElementById('custom-alert');
    if (type == "success") {
        str_html = "<div class='alert alert-success alert-dismissible fade show' role='alert'><strong>Success! </strong>" + message + "<button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button></div>";
        custom_alert_area.innerHTML = str_html;
    } else if (type == "error") {
        str_html = "<div class='alert alert-danger alert-dismissible fade show' role='alert'><strong>Error! </strong>" + message + "<button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button></div>";
        custom_alert_area.innerHTML = str_html;
    }
}