window.onload = function() {
    init();
}

// init
function init() {
    var signout_btn_clicked = false;
    // check user's sign state
    firebase.auth().onAuthStateChanged(function (user) {
        var account = document.getElementById('side-bar-account');
        var shopping_cart = document.getElementById('side-bar-shopping-cart');
        var historical_record_body = document.getElementById('historical-record-page-body-container');
        if(user) {
            // if user is in signin state
            account.innerHTML = '<a class="nav-link dropdown-toggle" data-toggle="dropdown">' + user.email + '</a>' + 
                                '<div id="side-bar-account-menu" class="dropdown-menu">' + 
                                  '<a id="homepage-btn" class="dropdown-item">Home</a>' + 
                                  '<a id="dashboard-btn" class="dropdown-item">Dashboard</a>' + 
                                  '<a id="signout-btn" class="dropdown-item">Signout</a>' + 
                                '</div>';
            shopping_cart.style.visibility = 'visible';
            historical_record_body.style.visibility = 'visible';
            
            // add event listener to homepage button
            var homepage_btn = document.getElementById('homepage-btn');
            homepage_btn.addEventListener('click', function() {
                window.location.assign("index.html");
            });

            // add event listener to dashboard button
            var dashboard_btn = document.getElementById('dashboard-btn');
            dashboard_btn.addEventListener('click', function() {
                window.location.assign("dashboard.html");
            });

            // add event listener to signout button
            var signout_btn = document.getElementById('signout-btn');
            signout_btn.addEventListener('click', function() {
                firebase.auth().signOut().then(function() {
                    signout_btn_clicked = true;
                    create_custom_alert('success', 'Successfully signout. Automatically redirecting to the Signin Page.')
                    setTimeout(function() {
                        window.location.assign("signin.html");
                    }, 1000);
                }).catch(function(error) {
                    var errorCode= error.code;
                    var errorMessage= error.message;
                    create_custom_alert('error', errorMessage);
                });
            });

            // load user historical shopping cart record
            var historical_shopping_cart_record_list_null = true;
            var historical_shopping_cart_record_list = document.getElementById('historical-shopping-cart-record-list');
            var historical_shopping_cart_record_html_list = [];
            var userShoppingCartListRef = firebase.database().ref('user_shopping_cart_list');
            userShoppingCartListRef.once('value').then(function(snapshot) {
                var userEmail = user.email;
                snapshot.forEach(function(childsnapshot) {
                    if(childsnapshot.val().email == userEmail) {
                        historical_shopping_cart_record_list_null = false;
                        
                        var historical_shopping_cart_record_html = historical_shopping_cart_record_html_code_generator(childsnapshot.val().shopping_cart_good_list, childsnapshot.val().shopping_cart_total_price, childsnapshot.val().shopping_cart_payment_serial_number);
                        historical_shopping_cart_record_html_list.push(historical_shopping_cart_record_html);
                    }
                    // update historical shopping cart record list
                    historical_shopping_cart_record_list.innerHTML = historical_shopping_cart_record_html_list.join('');
                });

                if(historical_shopping_cart_record_list_null == true) {
                    document.getElementById('historical-shopping-cart-record-list-null-alert').style.display = 'block';
                }
                document.getElementById('historical-shopping-cart-record-list-loading-alert').style.display = 'none';
            }).catch(function(error) {
                var errorCode= error.code;
                var errorMessage= error.message;
                create_custom_alert('error', errorMessage);
            });
        }
        else {
            // if user is in signout state
            account.innerHTML = '<a class="nav-link dropdown-toggle" data-toggle="dropdown">Guest</a>' + 
                                '<div id="side-bar-account-menu" class="dropdown-menu">' + 
                                  '<a id="homepage-btn" class="dropdown-item" href="index.html">Home</a>' + 
                                  '<a id="signin-btn" class="dropdown-item" href="signin.html">Signin</a>' + 
                                '</div>';
            shopping_cart.style.visibility = 'hidden';
            historical_record_body.style.visibility = 'hidden';
            if(!signout_btn_clicked) {
                create_authorization_alert('error', 'You should signin first to view this page.')
            }
        }
    });

    // add event listener to historical shopping cart record return button
    var historical_shopping_cart_record_return_btn = document.getElementById('historical-shopping-cart-record-return-btn');
    historical_shopping_cart_record_return_btn.addEventListener('click', function() {
        window.location.assign("dashboard.html");
    });
}

// historical shopping cart record html code generator
function historical_shopping_cart_record_html_code_generator(shopping_cart_good_list, shopping_cart_total_price, shopping_cart_payment_serial_number) {
    var str_1 = '<div class="historical-shopping-cart-record-shopping-cart">' + 
                  '<div class="historical-shopping-cart-record-shopping-cart-title-container">' + 
                    '<div class="historical-shopping-cart-record-shopping-cart-title">' + 
                      'Shopping Cart Record' + 
                    '</div>' + 
                  '</div>';
    var str_2 = '</div>';
    
    var historical_shopping_cart_record_good_list_html = historical_shopping_cart_record_good_list_html_code_generator(shopping_cart_good_list);
    var historical_shopping_cart_record_check_html = historical_shopping_cart_record_check_html_code_generator(shopping_cart_total_price);
    var historical_shopping_cart_record_payment_html = historical_shopping_cart_record_payment_html_code_generator(shopping_cart_payment_serial_number);

    return str_1 + historical_shopping_cart_record_good_list_html + historical_shopping_cart_record_check_html + historical_shopping_cart_record_payment_html + str_2;
}

// historical shopping cart record good list html code generator
function historical_shopping_cart_record_good_list_html_code_generator(shopping_cart_good_list) {
    var str_1 = '<div class="historical-shopping-cart-record-shopping-cart-good-list-container">' + 
                  '<div class="historical-shopping-cart-record-shopping-cart-good-list-title">' + 
                    'Good List :' + 
                  '</div>' + 
                  '<div class="historical-shopping-cart-record-shopping-cart-good-list">';
                    /*shopping cart good list*/
    var str_2 =   '</div>' + 
                '</div>';
    
    var shopping_cart_good_html_list = []
    for(var i = 0; i < shopping_cart_good_list.length; i++) {
        var shopping_cart_good_html = historical_shopping_cart_record_good_html_code_generator(shopping_cart_good_list[i]);
        shopping_cart_good_html_list.push(shopping_cart_good_html);
    }
    var shopping_cart_good_list_html = shopping_cart_good_html_list.join('');

    return str_1 + shopping_cart_good_list_html + str_2;
}

// historical shopping cart record good html code generator
function historical_shopping_cart_record_good_html_code_generator(shopping_cart_good) {
    var str_1 = '<div class="historical-shopping-cart-record-shopping-cart-good">' + 
                  '<div class="historical-shopping-cart-record-shopping-cart-good-left">' + 
                    '<span class="historical-shopping-cart-record-shopping-cart-good-name">'; /*good name*/
    var str_2 =                                                                                            '</span>' + 
                    '<span class="historical-shopping-cart-record-shopping-cart-good-description"> x </span>' + 
                    '<span class="historical-shopping-cart-record-shopping-cart-good-num">'; /*good num*/
    var str_3 =                                                                                          '</span>' + 
                  '</div>' + 
                  '<div class="historical-shopping-cart-record-shopping-cart-good-right">' + 
                    '<span class="historical-shopping-cart-record-shopping-cart-good-price">'; /*good price*/
    var str_4 =                                                                                              '</span>' + 
                    '<span class="historical-shopping-cart-record-shopping-cart-good-description"> 元</span>' + 
                  '</div>' + 
                '</div>';
    
    return str_1 + shopping_cart_good.good_name + str_2 + shopping_cart_good.good_num + str_3 + shopping_cart_good.good_price + str_4;
}

// historical shopping cart record check html code generator
function historical_shopping_cart_record_check_html_code_generator(shopping_cart_total_price) {
    var str_1 = '<div class="historical-shopping-cart-record-shopping-cart-check-container">' + 
                  '<hr class="historical-shopping-cart-record-shopping-cart-horizontal-line">' + 
                  '<div class="historical-shopping-cart-record-shopping-cart-total-price-container">' + 
                    '<div class="historical-shopping-cart-record-shopping-cart-total-price-left">' + 
                      '<span class="historical-shopping-cart-record-shopping-cart-total-price-left-description">Total Price :</span>' + 
                    '</div>' + 
                    '<div class="historical-shopping-cart-record-shopping-cart-total-price-right">' + 
                      '<span class="historical-shopping-cart-record-shopping-cart-total-price">'; /*total price*/
    var str_2 =                                                                                                  '</span>' + 
                      '<span class="historical-shopping-cart-record-shopping-cart-total-price-right-description"> 元</span>' + 
                    '</div>' + 
                  '</div>' + 
                '</div>';
    
    return str_1 + shopping_cart_total_price + str_2;
}

// historical shopping cart record payment html code generator
function historical_shopping_cart_record_payment_html_code_generator(shopping_cart_payment_serial_number) {
    var str_1 = '<div class="historical-shopping-cart-record-shopping-cart-payment-container">' + 
                  '<hr class="historical-shopping-cart-record-shopping-cart-horizontal-line">' + 
                  '<div class="historical-shopping-cart-record-shopping-cart-payment-serial-number-container">' + 
                    '<span class="historical-shopping-cart-record-shopping-cart-payment-serial-number-description">Payment Serial Number : </span>' + 
                    '<span class="historical-shopping-cart-record-shopping-cart-payment-serial-number">'; /*serial number*/
    var str_2 =                                                                                                            '</span>' + 
                  '</div>' + 
                '</div>';

    return str_1 + shopping_cart_payment_serial_number + str_2;
}

// custom alert
function create_custom_alert(type, message) {
    var custom_alert_area = document.getElementById('custom-alert');
    if (type == "success") {
        str_html = "<div class='alert alert-success alert-dismissible fade show' role='alert'><strong>Success! </strong>" + message + "<button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button></div>";
        custom_alert_area.innerHTML = str_html;
    } else if (type == "error") {
        str_html = "<div class='alert alert-danger alert-dismissible fade show' role='alert'><strong>Error! </strong>" + message + "<button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button></div>";
        custom_alert_area.innerHTML = str_html;
    }
}

// authorization alert area
function create_authorization_alert(type, message) {
    var authorization_alert_area = document.getElementById('authorization-alert');
    if (type == "success") {
        str_html = "<div class='alert alert-success alert-dismissible fade show' role='alert'><strong>Authorization Agreed! </strong>" + message + "<button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button></div>";
        authorization_alert_area.innerHTML = str_html;
    } else if (type == "error") {
        str_html = "<div class='alert alert-danger alert-dismissible fade show' role='alert'><strong>Authorization Denied! </strong>" + message + "<button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button></div>";
        authorization_alert_area.innerHTML = str_html;
    }
}