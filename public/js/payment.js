window.onload = function() {
    init();
}

// init
function init() {
    var signout_btn_clicked = false;
    // check user's sign state
    firebase.auth().onAuthStateChanged(function (user) {
        var account = document.getElementById('side-bar-account');
        var shopping_cart = document.getElementById('side-bar-shopping-cart');
        var payment_body = document.getElementById('payment-page-body-container');
        if(user) {
            // if user is in signin state
            account.innerHTML = '<a class="nav-link dropdown-toggle" data-toggle="dropdown">' + user.email + '</a>' + 
                                '<div id="side-bar-account-menu" class="dropdown-menu">' + 
                                  '<a id="homepage-btn" class="dropdown-item">Home</a>' + 
                                  '<a id="dashboard-btn" class="dropdown-item">Dashboard</a>' + 
                                  '<a id="signout-btn" class="dropdown-item">Signout</a>' + 
                                '</div>';
            shopping_cart.style.visibility = 'visible';
            payment_body.style.visibility = 'visible';
            
            // add event listener to homepage button
            var homepage_btn = document.getElementById('homepage-btn');
            homepage_btn.addEventListener('click', function() {
                window.location.assign("index.html");
            });

            // add event listener to dashboard button
            var dashboard_btn = document.getElementById('dashboard-btn');
            dashboard_btn.addEventListener('click', function() {
                window.location.assign("dashboard.html");
            });

            // add event listener to signout button
            var signout_btn = document.getElementById('signout-btn');
            signout_btn.addEventListener('click', function() {
                firebase.auth().signOut().then(function() {
                    signout_btn_clicked = true;
                    create_custom_alert('success', 'Successfully signout. Automatically redirecting to the Signin Page.')
                    setTimeout(function() {
                        window.location.assign("signin.html");
                    }, 1000);
                }).catch(function(error) {
                    var errorCode= error.code;
                    var errorMessage= error.message;
                    create_custom_alert('error', errorMessage);
                });
            });
        }
        else {
            // if user is in signout state
            account.innerHTML = '<a class="nav-link dropdown-toggle" data-toggle="dropdown">Guest</a>' + 
                                '<div id="side-bar-account-menu" class="dropdown-menu">' + 
                                  '<a id="homepage-btn" class="dropdown-item" href="index.html">Home</a>' + 
                                  '<a id="signin-btn" class="dropdown-item" href="signin.html">Signin</a>' + 
                                '</div>';
            shopping_cart.style.visibility = 'hidden';
            payment_body.style.visibility = 'hidden';
            if(!signout_btn_clicked) {
                create_authorization_alert('error', 'You should signin first to view this page.')
            }
        }
    });

    // update payment information html code
    var payment_information_good_list = document.getElementById('payment-information-good-list-container');
    var userShoppingCartListRef = firebase.database().ref('user_shopping_cart_list');
    userShoppingCartListRef.once('value').then(function(snapshot) {
        var childsnapshot_list = [];
        snapshot.forEach(function(childsnapshot) {
            var childsnapshot_list_item = {
                email: childsnapshot.val().email,
                shopping_cart_list: childsnapshot.val().shopping_cart_good_list,
                shopping_cart_list_total_price: childsnapshot.val().shopping_cart_total_price,
                payment_serial_number: childsnapshot.val().shopping_cart_payment_serial_number
            }
            childsnapshot_list.push(childsnapshot_list_item);
        });

        var newest_childsnapshot_list_item = childsnapshot_list[childsnapshot_list.length-1];
        
        // update payment information good list html code
        for(var i = 0; i < newest_childsnapshot_list_item.shopping_cart_list.length; i++) {
            var payment_information_good_list_good_html_code = payment_information_good_list_good_html_generator(newest_childsnapshot_list_item.shopping_cart_list[i].good_name, newest_childsnapshot_list_item.shopping_cart_list[i].good_num, newest_childsnapshot_list_item.shopping_cart_list[i].good_price);
            payment_information_good_list.innerHTML += payment_information_good_list_good_html_code;
        }

        // update payment information check total price html code
        var payment_information_check_total_price = document.getElementById('payment-information-check-total-price');
        payment_information_check_total_price.innerHTML = newest_childsnapshot_list_item.shopping_cart_list_total_price;

        // update payment serial number html code
        var payment_serial_number = document.getElementById('payment-serial-number');
        payment_serial_number.innerHTML = newest_childsnapshot_list_item.payment_serial_number;
    }).catch(function(error) {
        var errorCode = error.code;
        var errorMessage = error.message;
        create_custom_alert('error', errorMessage);
    });

    // add event listener to payment information return button
    var payment_information_return_btn = document.getElementById('payment-information-return-btn');
    payment_information_return_btn.addEventListener('click', function() {
        window.location.assign("index.html");
    });
}

// payment information good list good html generator
function payment_information_good_list_good_html_generator(good_name, good_num, good_price) {
    var str_1 = '<div class="payment-information-good">' + 
                  '<div class="payment-information-good-left">' + 
                    '<span class="payment-information-good-name">'; /*good_name*/
    var str_2 =                                                                  '</span>' + 
                    '<span class="payment-information-good-description"> x </span>' + 
                    '<span class="payment-information-good-num">'; /*good_num*/
    var str_3 =                                                                '</span>' + 
                  '</div>' + 
                  '<div class="payment-information-good-right">' + 
                    '<span class="payment-information-good-price">'; /*good_price*/
    var str_4 =                                                                    '</span>' + 
                    '<span class="payment-information-good-description"> 元</span>' + 
                  '</div>' + 
                '</div>';
    return str_1 + good_name + str_2 + good_num + str_3 + good_price + str_4;
}

// custom alert
function create_custom_alert(type, message) {
    var custom_alert_area = document.getElementById('custom-alert');
    if (type == "success") {
        str_html = "<div class='alert alert-success alert-dismissible fade show' role='alert'><strong>Success! </strong>" + message + "<button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button></div>";
        custom_alert_area.innerHTML = str_html;
    } else if (type == "error") {
        str_html = "<div class='alert alert-danger alert-dismissible fade show' role='alert'><strong>Error! </strong>" + message + "<button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button></div>";
        custom_alert_area.innerHTML = str_html;
    }
}

// authorization alert area
function create_authorization_alert(type, message) {
    var authorization_alert_area = document.getElementById('authorization-alert');
    if (type == "success") {
        str_html = "<div class='alert alert-success alert-dismissible fade show' role='alert'><strong>Authorization Agreed! </strong>" + message + "<button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button></div>";
        authorization_alert_area.innerHTML = str_html;
    } else if (type == "error") {
        str_html = "<div class='alert alert-danger alert-dismissible fade show' role='alert'><strong>Authorization Denied! </strong>" + message + "<button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button></div>";
        authorization_alert_area.innerHTML = str_html;
    }
}