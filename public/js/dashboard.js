window.onload = function() {
    init();
}

// init
function init() {
    var signout_btn_clicked = false;
    // check user's sign state
    firebase.auth().onAuthStateChanged(function (user) {
        var account = document.getElementById('side-bar-account');
        var shopping_cart = document.getElementById('side-bar-shopping-cart');
        var dashboard_body = document.getElementById('dashboard-page-body-container');
        if(user) {
            // if user is in signin state
            account.innerHTML = '<a class="nav-link dropdown-toggle" data-toggle="dropdown">' + user.email + '</a>' + 
                                '<div id="side-bar-account-menu" class="dropdown-menu">' + 
                                  '<a id="homepage-btn" class="dropdown-item">Home</a>' + 
                                  '<a id="dashboard-btn" class="dropdown-item">Dashboard</a>' + 
                                  '<a id="signout-btn" class="dropdown-item">Signout</a>' + 
                                '</div>';
            shopping_cart.style.visibility = 'visible';
            dashboard_body.style.visibility = 'visible';
            
            // add event listener to homepage button
            var homepage_btn = document.getElementById('homepage-btn');
            homepage_btn.addEventListener('click', function() {
                window.location.assign("index.html");
            });

            // add event listener to dashboard button
            var dashboard_btn = document.getElementById('dashboard-btn');
            dashboard_btn.addEventListener('click', function() {
                window.location.reload();
            });

            // add event listener to signout button
            var signout_btn = document.getElementById('signout-btn');
            signout_btn.addEventListener('click', function() {
                firebase.auth().signOut().then(function() {
                    signout_btn_clicked = true;
                    create_custom_alert('success', 'Successfully signout. Automatically redirecting to the Signin Page.')
                    setTimeout(function() {
                        window.location.assign("signin.html");
                    }, 1000);
                }).catch(function(error) {
                    var errorCode= error.code;
                    var errorMessage= error.message;
                    create_custom_alert('error', errorMessage);
                });
            });

            // load user information
            var userFirstName = document.getElementById('user-name-first-name');
            var userLastName = document.getElementById('user-name-last-name');
            var userGender = document.getElementById('user-gender');
            var userAge = document.getElementById('user-age');
            var userPhone = document.getElementById('user-phone');
            var userInformationConfirm = document.getElementById('btnConfirm');
            
            var userInformationList = firebase.database().ref('user_information_list');
            userInformationList.once('value').then(function(snapshot) {
                var userInformationFirstSet = false;
                snapshot.forEach(function(childsnapshot) {
                    if(childsnapshot.val().email == user.email) {
                        // if user has first set user information
                        userInformationFirstSet = true;
                        
                        userFirstName.value = childsnapshot.val().firstname;
                        userLastName.value = childsnapshot.val().lastname;
                        userGender.value = childsnapshot.val().gender;
                        userAge.value = childsnapshot.val().age;
                        userPhone.value = childsnapshot.val().phone;

                        // update user information
                        userInformationConfirm.addEventListener('click', function() {
                            childsnapshot.ref.update({
                                firstname: userFirstName.value,
                                lastname: userLastName.value,
                                gender: userGender.value,
                                age: userAge.value,
                                phone: userPhone.value
                            });

                            create_custom_alert('success', 'Successfully update user information. Automatically reloading the current page.');
                            setTimeout(function() {
                                window.location.reload();
                            }, 1000);
                        });                        
                    }
                });

                if(!userInformationFirstSet) {
                    // if user hasn't first set user information
                    // first set user information
                    userInformationConfirm.addEventListener('click', function() {
                        var newUserInformationRef = firebase.database().ref('user_information_list').push();
                        newUserInformationRef.set({
                            // email: userEmail.value,
                            email: user.email,
                            firstname: userFirstName.value,
                            lastname: userLastName.value,
                            gender: userGender.value,
                            age: userAge.value,
                            phone: userPhone.value
                        });
                        
                        create_custom_alert('success', 'Successfully update user information. Automatically reloading the current page.');
                        setTimeout(function() {
                            window.location.reload();
                        }, 1000);
                    });
                }
            }).catch(function(error) {
                var errorCode= error.code;
                var errorMessage= error.message;
                create_custom_alert('error', errorMessage);
            });
        }
        else {
            // if user is in signout state
            account.innerHTML = '<a class="nav-link dropdown-toggle" data-toggle="dropdown">Guest</a>' + 
                                '<div id="side-bar-account-menu" class="dropdown-menu">' + 
                                  '<a id="homepage-btn" class="dropdown-item" href="index.html">Home</a>' + 
                                  '<a id="signin-btn" class="dropdown-item" href="signin.html">Signin</a>' + 
                                '</div>';
            shopping_cart.style.visibility = 'hidden';
            dashboard_body.style.visibility = 'hidden';
            if(!signout_btn_clicked) {
                create_authorization_alert('error', 'You should signin first to view this page.')
            }
        }
		
		// add event listener to historical shopping cart record button
        var historical_shopping_cart_record = document.getElementById('user-historical-shopping-cart-record-btn');
        historical_shopping_cart_record.addEventListener('click', function() {
			create_custom_alert('success', 'Automatically redirecting to the Historical Record Page.')
            setTimeout(function() {
                window.location.assign("historical_record.html");
            }, 1000);
        });

        // add event listener to good selling button
        var historical_shopping_cart_record = document.getElementById('user-good-selling-btn');
        historical_shopping_cart_record.addEventListener('click', function() {
			create_custom_alert('success', 'Automatically redirecting to the Good Selling Page.')
            setTimeout(function() {
                window.location.assign("good_selling.html");
            }, 1000);
        });
    });
}

// custom alert
function create_custom_alert(type, message) {
    var custom_alert_area = document.getElementById('custom-alert');
    if (type == "success") {
        str_html = "<div class='alert alert-success alert-dismissible fade show' role='alert'><strong>Success! </strong>" + message + "<button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button></div>";
        custom_alert_area.innerHTML = str_html;
    } else if (type == "error") {
        str_html = "<div class='alert alert-danger alert-dismissible fade show' role='alert'><strong>Error! </strong>" + message + "<button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button></div>";
        custom_alert_area.innerHTML = str_html;
    }
}

// authorization alert area
function create_authorization_alert(type, message) {
    var authorization_alert_area = document.getElementById('authorization-alert');
    if (type == "success") {
        str_html = "<div class='alert alert-success alert-dismissible fade show' role='alert'><strong>Authorization Agreed! </strong>" + message + "<button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button></div>";
        authorization_alert_area.innerHTML = str_html;
    } else if (type == "error") {
        str_html = "<div class='alert alert-danger alert-dismissible fade show' role='alert'><strong>Authorization Denied! </strong>" + message + "<button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button></div>";
        authorization_alert_area.innerHTML = str_html;
    }
}